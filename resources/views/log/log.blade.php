@extends('layouts.master')
@section('content')

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor" style="padding: 0.375rem 0.75rem;">Лог бүртгэл</h4>
        </div>

    </div>
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Хэрэглэгчийн лог бүртгэл</h4>
            <h6 class="card-subtitle">Copy, CSV, Excel, PDF & Print байдлаар мэдээлэл татаж авах боломжтой</h6>
            <div class="table-responsive m-t-40">
                <table id="myTable" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                    <thead class="text-center">
                    <tr>
                        <th>Хэрэглэгчийн нэр</th>
                        <th>Хийсэн үйлдэл</th>
                        <th>Тайлбар</th>
                        <th>Амжилттай болсон эсэх</th>
                        <th>Огноо</th>
                    </tr>
                    </thead>
                    <tbody class="text-center">
                    @foreach($AdminLogs as $adminLog)
                        <tr >
                            <td>{{ Auth::user()->name }}</td>
                            <td>{{ $adminLog->operation }}</td>
                            <td>{{ $adminLog->description }}</td>
                            @if($adminLog->status == 1)
                                <td><span class="label label-rounded label-info">амжилттай</span></td>
                            @else
                                <td><span class="label label-rounded label-info">амжилтгүй</span></td>
                            @endif

                            <td>{{ $adminLog->insert_time }}</td>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
@push('styles')
    <link href="/assets/node_modules/datatables/media/css/dataTables.bootstrap4.css" rel="stylesheet">
@endpush
@push('scripts')
    <script src="/assets/node_modules/datatables/datatables.min.js"></script>
    <script src="/dist/js/datatables.buttons.min.js"></script>
    <script src="/dist/js/buttons.flash.min.js"></script>
    <script src="/dist/js/jszip.min.js"></script>
    <script src="/dist/js/pdfmake.min.js"></script>
    <script src="/dist/js/vfs_fonts.js"></script>
    <script src="/dist/js/buttons.html5.min.js"></script>
    <script src="/dist/js/buttons.print.min.js"></script>

    <script>
        $(function() {
            $('#myTable').DataTable({
                dom: 'Bfrtip',
                "language" : {
                    "emptyTable":     "Мэдээлэл олдсонгүй",
                    "info":           "Дэлгэцэнд _START_ to _END_ of _TOTAL_ хооронд харуулж байна",
                    "infoEmpty":      "Дэлгэцэнд 0 to 0 of 0 хооронд харуулж байна",
                    "infoFiltered":   "(Нийт _MAX_ мэдээллээс шүүлт хийв)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Дэлгэцлэх _MENU_ мөрийн тоо",
                    "loadingRecords": "Ачааллаж байна...",
                    "processing":     "Тооцоолж байна...",
                    "search":         "Хайх:",
                    "zeroRecords":    "Хайлтын үр дүн олдсонгүй",
                    "paginate": {
                        "first":      "Эхэнд",
                        "last":       "Сүүлд",
                        "next":       "Дараагийн хуудас",
                        "previous":   "Өмнөх хуудас"
                    },
                    "aria": {
                        "sortAscending":  ": Өсөх дарааллаар эрэмблэх",
                        "sortDescending": ": Буурах дарааллаар эрэмблэх"
                    },
                    "fnDrawCallback": function( oSettings ) {
                        alert( 'DataTables has redrawn the table' );
                    }
                },
                "order": [[ 4, "desc" ]],
                "pageLength": 20,
                "lengthMenu": [[20, 40, 60, -1], [20, 40, 60, "All"]],
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
            $('.buttons-copy, .buttons-csv, .buttons-print, .buttons-pdf, .buttons-excel').addClass('btn btn-primary mr-1');
        });

    </script>
@endpush