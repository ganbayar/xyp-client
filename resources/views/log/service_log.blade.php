@extends('layouts.master')
@section('content')

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor" style="padding: 0.375rem 0.75rem;">Лог бүртгэл</h4>
        </div>

    </div>
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Сервисийн лог бүртгэл</h4>
            <h6 class="card-subtitle">Copy, CSV, Excel, PDF & Print байдлаар мэдээлэл татаж авах боломжтой</h6>
            <div class="table-responsive m-t-40">
                <table id="myTable" class="table table-bordered table-striped" width="100%">
                    <thead class="text-center">
                    <tr>
                        <th>Хүсэлт илгээсэн огноо</th>
                        <th>Хүсэлтийн дугаар</th>
                        <th>Иргэний регистрийн дугаар</th>
                        <th>Үйлчилгээний ажилтны регистрийн дугаар</th>
                        <th>Ашигласан сервис</th>
                        <th>Үйлчилгээний дүн</th>
                        <th width="200">Статус</th>
                    </tr>
                    </thead>
                    <tbody class="text-center">
                    @foreach($logs as $log)
                        <tr>
                            <td>{{ $log->request_timestamp }}</td>
                            <td>{{ $log->request_id }}</td>
                            <td>{{ $log->citizen_regnum }}</td>
                            <td>{{ $log->operator_regnum }}</td>
                            {{--<td>{{ $log->raw_data }}</td>--}}
                            <td>{{ $log->wsdl }}</td>
                            <td>{{ $log->billed_amount }}</td>
                            <td>
                                @switch($log->status)
                                    @case(0) <label class="label label-rounded label-info">амжилттай</label> @break
                                    @case(1) <label class="label label-rounded label-danger">Үр дүн олдсонгүй</label> @break
                                    @case(2) <label class="label label-rounded label-danger">Шалгалтын явцад алдаа гарлаа</label> @break
                                    @case(3) <label class="label label-rounded label-danger">Баталгаажуулалт буруу</label> @break
                                    @case(301) <label class="label label-rounded label-danger">Хурууны хээ олдсонгүй</label> @break
                                    @case(302) <label class="label label-rounded label-danger">Мэдээлэл зөрүүтэй байна</label> @break
                                    @case(303) <label class="label label-rounded label-danger">Хурууны хээ тулгах процесс хэт удаан</label> @break
                                    @case(304) <label class="label label-rounded label-danger">Хурууны хээ тулгахад алдаа гарлаа</label> @break
                                    @case(402) <label class="label label-rounded label-danger">Ззэмшигчийн мэдээлэл зөрүүтэй</label> @break
                                    @default <label class="label label-rounded label-danger">Алдаа</label>
                                @endswitch
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
@push('styles')
    <link href="/assets/node_modules/datatables/media/css/dataTables.bootstrap4.css" rel="stylesheet">
@endpush
@push('scripts')
    <script src="/assets/node_modules/datatables/datatables.min.js"></script>
    <script src="/dist/js/datatables.buttons.min.js"></script>
    <script src="/dist/js/buttons.flash.min.js"></script>
    <script src="/dist/js/jszip.min.js"></script>
    <script src="/dist/js/pdfmake.min.js"></script>
    <script src="/dist/js/vfs_fonts.js"></script>
    <script src="/dist/js/buttons.html5.min.js"></script>
    <script src="/dist/js/buttons.print.min.js"></script>
    <script>
        $(function() {
            $('#myTable').DataTable({
                dom: 'Bfrtip',
                "language" : {
                    "emptyTable":     "Мэдээлэл олдсонгүй",
                    "info":           "Дэлгэцэнд _START_ to _END_ of _TOTAL_ хооронд харуулж байна",
                    "infoEmpty":      "Дэлгэцэнд 0 to 0 of 0 хооронд харуулж байна",
                    "infoFiltered":   "(Нийт _MAX_ мэдээллээс шүүлт хийв)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Дэлгэцлэх _MENU_ мөрийн тоо",
                    "loadingRecords": "Ачааллаж байна...",
                    "processing":     "Тооцоолж байна...",
                    "search":         "Хайх:",
                    "zeroRecords":    "Хайлтын үр дүн олдсонгүй",
                    "paginate": {
                        "first":      "Эхэнд",
                        "last":       "Сүүлд",
                        "next":       "Дараагийн хуудас",
                        "previous":   "Өмнөх хуудас"
                    },
                    "aria": {
                        "sortAscending":  ": Өсөх дарааллаар эрэмблэх",
                        "sortDescending": ": Буурах дарааллаар эрэмблэх"
                    },
                    "fnDrawCallback": function( oSettings ) {
                        alert( 'DataTables has redrawn the table' );
                    }
                },
                "order": [[ 0, "desc" ]],
                "pageLength": 20,
                "lengthMenu": [[20, 40, 60, -1], [20, 40, 60, "All"]],
                buttons: [
                    'copy', 'csv', 'excel', 'print',{
                        extend: 'pdf',
                        orientation: 'landscape',
                        pageSize: 'LEGAL'
                    }
                ]
            });
            $('.buttons-copy, .buttons-csv, .buttons-print, .buttons-pdf, .buttons-excel').addClass('btn btn-primary mr-1');
        });
    </script>
@endpush