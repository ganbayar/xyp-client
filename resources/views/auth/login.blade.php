<!DOCTYPE html>
<html lang="lang="{{ config('app.locale') }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="/images/{{ config('app.app_fav_icon') }}">
    <title>{{ config('app.app_org_title') }}</title>

    <!-- page css -->
    <link href="/dist/css/pages/login-register-lock.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="/dist/css/style.min.css" rel="stylesheet">

    <link href="/assets/node_modules/toast-master/css/jquery.toast.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        .btn-info {
            color: #fff;
            background: {{config('app.app_theme')}}  !important;
            border-color: {{config('app.app_theme_body')}} !important;
        }
    </style>
</head>

<body>
<!-- ============================================================== -->
<!-- Preloader - style you can find in spinners.css -->
<!-- ============================================================== -->
<div class="preloader">
    <div class="loader">
        <div class="loader__figure"></div>
        <p class="loader__label">{{ config('app.app_org_short_name') }}</p>
    </div>
</div>
<!-- ============================================================== -->
<!-- Main wrapper - style you can find in pages.scss -->
<!-- ============================================================== -->
<section id="wrapper" class="login-register login-sidebar" style="background-image:url(/images/{{ config('app.app_background_image') }}); background-size: calc(100% - 400px) 100%; background-position: left; width: 100%;" >
    <div class="login-box card">
        <div class="card-body">
            <form class="form-horizontal form-material text-center" id="loginform" action="{{ route('login') }}" method="post">
                @csrf
                <a href="javascript:void(0)" class="db"><img src="/images/{{ config('app.app_logo') }}" alt="Home" height="80" /><br/></a>
                <div class="form-group m-t-40 {{ $errors->has('username') ? ' error' : '' }}">
                    <div class="col-xs-12">
                        <input class="form-control" type="text" name="username" required="" placeholder="Хэрэглэгчийн нэр">
                        @if ($errors->has('username'))
                            <div class="help-block"><ul role="alert"><li>{{ $errors->first('username') }}</ul></div>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <input class="form-control" type="password" name="password" required="" placeholder="Нууц үг">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-12">
                        <div class="d-flex no-block align-items-center">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="customCheck1">
                                <label class="custom-control-label" for="customCheck1">Намайг сана</label>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="form-group text-center m-t-20">
                    <div class="col-xs-12">
                        <button class="btn btn-info btn-lg btn-block text-uppercase btn-rounded" type="submit">Нэвтрэх</button>
                    </div>
                </div>

            </form>

        </div>
        <p class="text-center login-copyright" style="font-size: 11px;">
            <a href="https://esign.gov.mn/storage/app/media/uploaded-files/xypfingerprint.zip" target="_blank">Хурууны хээний программ татаж авах</a>
            <br>
            © 2018 Бүх эрх хуулиар хамгаалагдсан.  <br><a href="{{ config('app.app_link') }}" target="_blank" class="gov-color">{{ config('app.app_org_title') }}</a></p>
    </div>
</section>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->
<script src="/assets/node_modules/jquery/jquery-3.2.1.min.js"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="/assets/node_modules/popper/popper.min.js"></script>
<script src="/assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="/assets/node_modules/toast-master/js/jquery.toast.js"></script>
<script>
    @if(Session::has('error'))
    $(function() {
        $.toast({
            heading: 'Алдаа',
            text: "{{Session::get('error')}}",
            position: 'bottom-right',
            loaderBg:'#ff6849',
            icon: 'error',
            hideAfter: 3500

        });
    });
    @endif
    @if(Session::has('success'))
    $(function() {
        $.toast({
            heading: 'Амжилттай',
            text: "{{Session::get('success')}}",
            position: 'bottom-right',
            loaderBg:'#ff6849',
            icon: 'success',
            hideAfter: 3500

        });
    });
    {{Session::forget('success')}}
    @endif
</script>
<!--Custom JavaScript -->
<script type="text/javascript">
    $(function() {
        $(".preloader").fadeOut();
    });
    $(function() {
        $('[data-toggle="tooltip"]').tooltip()
    });
    // ==============================================================
    // Login and Recover Password
    // ==============================================================
    $('#to-recover').on("click", function() {
        $("#loginform").slideUp();
        $("#recoverform").fadeIn();
    });
</script>

</body>

</html>
