<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/{{ config('app.app_fav_icon') }}">
    <title>{{ config('app.app_org_title') }}</title>
    <!-- Bootstrap Core CSS -->

    <!--Toaster Popup message CSS -->
    <link href="/assets/node_modules/toast-master/css/jquery.toast.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="/dist/css/style.min.css" rel="stylesheet">
    @stack('styles')
    <!-- Dashboard 1 Page CSS -->
    <link href="/dist/css/pages/dashboard1.css" rel="stylesheet">
    <link rel="stylesheet" href="/dist/css/custom.css">
    <style>
        .skin-default .topbar{background:{{config('app.app_theme')}} !important;}
        .bg-info {
            background: {{config('app.app_theme')}}!important;
        }
        .btn-info {
            color: #fff;
            background-color: {{config('app.app_theme_body')}}  !important;
            border-color: {{config('app.app_theme_body')}} !important;
        }
        .label-info {
            background: {{config('app.app_theme')}} !important;
        }
        .btn-choose{
            background-color: #fff; color: {{config('app.app_theme_body')}}; font-weight: 600;
        }
    </style>
</head>

<body class="skin-default fixed-layout">
<!-- Preloader -->
<div class="preloader">
    <div class="loader">
        <div class="loader__figure"></div>
        <p class="loader__label">{{ config('app.app_org_short_name') }}</p>
    </div>
</div>
<div id="main-wrapper">    <!-- Navigation -->
    @include('includes.header')
    <!-- Left navbar-header -->
    @include('includes.sidebar')
    <!-- Left navbar-header end -->
    <!-- Page Content    -->
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
            @yield('content')
            <!-- /.row -->
            <!-- .right-sidebar -->
            @include('includes.right-sidebar')
            <!-- /.right-sidebar -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->
    <footer class="footer text-center"> 2018 &copy; XYP client</footer>
</div>
<!-- /#wrapper -->
<!-- jQuery -->

<script src="/assets/node_modules/jquery/jquery-3.2.1.min.js"></script>

<!-- Bootstrap popper Core JavaScript -->
<script src="/assets/node_modules/popper/popper.min.js"></script>
<script src="/assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="/dist/js/perfect-scrollbar.jquery.min.js"></script>
<!--Wave Effects -->
<script src="/dist/js/waves.js"></script>
<!--Menu sidebar -->
<script src="/dist/js/sidebarmenu.js"></script>
<!--Custom JavaScript -->
<script src="/dist/js/custom.min.js"></script>
<!-- ============================================================== -->
<!-- This page plugins -->
<!-- ============================================================== -->
<!--morris JavaScript -->

{{--<script src="/assets/node_modules/jquery-sparkline/jquery.sparkline.min.js"></script>--}}
<!-- Popup message jquery -->
<script src="/assets/node_modules/toast-master/js/jquery.toast.js"></script>
<!-- Chart JS -->
<script>
    @if(Session::has('error'))
    $(function() {
        $.toast({
            heading: 'Алдаа',
            text: "{{Session::get('error')}}",
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'error',
            hideAfter: 3500

        });
    });
    @endif
    @if(Session::has('success'))
    $(function() {
        $.toast({
            heading: 'Амжилттай',
            text: "{{Session::get('success')}}",
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'success',
            hideAfter: 3500

        });
    });
    {{Session::forget('success')}}
    @endif
</script>
@stack('scripts')
</body>

</html>
