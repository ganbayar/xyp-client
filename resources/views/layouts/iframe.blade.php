<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/images/favicon.png">
    <title>ХУР систем</title>
    <!-- Bootstrap Core CSS -->
    <link href="/assets/node_modules/morrisjs/morris.css" rel="stylesheet">
    <!--Toaster Popup message CSS -->
    <!-- Custom CSS -->
    <link href="/dist/css/style.min.css" rel="stylesheet">
    <link href="/assets/node_modules/multiselect/css/multi-select.css" rel="stylesheet" type="text/css" />
    <style>
        .ms-container{
            width: 100% !important;
        }
    </style>
    <script src="/assets/node_modules/jquery/jquery-3.2.1.min.js"></script>

    <!-- Bootstrap popper Core JavaScript -->
    <script src="/assets/node_modules/popper/popper.min.js"></script>
    <script src="/assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="/dist/js/perfect-scrollbar.jquery.min.js"></script>
    <!--Wave Effects -->
    <script src="/dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="/dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <!--morris JavaScript -->
    <script src="/assets/node_modules/morrisjs/morris.min.js"></script>
    <script src="/assets/node_modules/raphael/raphael-min.js"></script>
    <script src="/assets/node_modules/jquery-sparkline/jquery.sparkline.min.js"></script>
    <script type="text/javascript" src="/assets/node_modules/multiselect/js/jquery.multi-select.js"></script>

    <!-- Popup message jquery -->
    <!-- Chart JS -->
    <script>
        $(document).ready(function() {
            $('#pre-selected-options').multiSelect({

                afterSelect: function(){
                    this.qs1.cache();
                    this.qs2.cache();
                },
                afterDeselect: function(){
                    this.qs1.cache();
                    this.qs2.cache();
                }
            });
            $('#optgroup').multiSelect({
                selectableOptgroup: true
            });
            $('#pre-selected-options').multiSelect();
            $('#select-all').click(function () {
                $('#pre-selected-options').multiSelect('select_all');
                return false;
            });
            $('#deselect-all').click(function () {
                $('#pre-selected-options').multiSelect('deselect_all');
                return false;
            });
        });



    </script>
</head>

<body style="background-color: #fff;">
<div class="button-box m-b-20">
    <a id="select-all" class="btn btn-danger" href="javascript:void(0)">Бүгдийг сонгох</a>
    <a id="deselect-all" class="btn btn-info" href="javascript:void(0)">Бүгдийг цуцлах</a>
</div>
@yield('content')
</body>

</html>
