<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ public_path('/images/favicon.png') }}">
    <title>ИНВЕСКОР Санхүүгийн байгууллага</title>
    <!-- Bootstrap Core CSS -->


    <!-- Custom CSS -->
    <link href="{{ public_path('/dist/css/style-pdf.min.css') }}" rel="stylesheet">
@stack('styles')
<!-- Dashboard 1 Page CSS -->

    <link rel="stylesheet" href="{{ public_path('/dist/css/custom.css') }}">
    <style>

        @page {
            height: 297mm;
            width: 210mm;
        }
        .service-legal table{
            font-size: 10px !important;

        }
        .service-legal .table td,.service-legal .table th{
            padding: 3px 1px !important;
        }
        .tab-pane {
            display: block !important;
            page-break-after: always;

            width: 210mm;
            position: relative;

        }
        .service-footer {
            position: relative;
            width: 100%;
            page-break-inside: avoid;
        }
        .fixed-layout .page-wrapper{
            padding-top: 0;
            margin-left: 0;
            margin-bottom: 0;
        }
        .container-fluid{
            padding: 0;
            margin-bottom: 0;
        }
        .vtabs .tab-content{
            width: 100%;
            margin-bottom: 0;
        }
        .card{
            margin-bottom: 0;
        }
        .card-body{
            padding-bottom: 0 !important;
        }
        .tab-content {
            padding-bottom: 0 !important;
        }

    </style>
</head>

<body class="skin-default fixed-layout">
<!-- Preloader -->

<div id="main-wrapper">    <!-- Navigation -->

<!-- Left navbar-header end -->
    <!-- Page Content -->
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
        @yield('content')
        <!-- /.row -->
            <!-- .right-sidebar -->

        <!-- /.right-sidebar -->
        </div>
        <!-- /.container-fluid -->
    </div>

</div>
<!-- /#wrapper -->
<!-- jQuery -->

{{--<script src="/assets/node_modules/jquery/jquery-3.2.1.min.js"></script>--}}

{{--<!-- Bootstrap popper Core JavaScript -->--}}
{{--<script src="/assets/node_modules/popper/popper.min.js"></script>--}}
{{--<script src="/assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>--}}
{{--<!-- slimscrollbar scrollbar JavaScript -->--}}
{{--<script src="/dist/js/perfect-scrollbar.jquery.min.js"></script>--}}
{{--<!--Wave Effects -->--}}

{{--<!--Custom JavaScript -->--}}
{{--<script src="/dist/js/custom.min.js"></script>--}}
<!-- ============================================================== -->
<!-- This page plugins -->
<!-- ============================================================== -->
<!--morris JavaScript -->

{{--<script src="/assets/node_modules/jquery-sparkline/jquery.sparkline.min.js"></script>--}}
<!-- Popup message jquery -->

@stack('scripts')
</body>

</html>
