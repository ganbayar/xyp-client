@extends('layouts.master')
@section('content')


<div class="toggler">
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor" style="padding: 0.375rem 0.75rem;">Оролтын хуудас</h4>
        </div>

    </div>
    <div class="row justify-content-center">
        <div class="col-md-5 col-lg-5">
            <div class="card border-info">
                <div class="card-header bg-info">
                    <h4 class="m-b-0 text-white">Оролт</h4>
                </div>
                <div class="card-body">
                    <form action="" method="POST" class="form-material" autocomplete="off">
                        {{ csrf_field() }}
                        @foreach($inputList as $param)
                            @if($param->getType() != 'hidden')
                            <div class="form-group {{ $errors->has($param->getName())?'has-danger':'' }}">
                                <label for="">{{ $param->getLabel() }}</label>
                            @endif
                                @if($param->getName() == 'endYear' || $param->getName() == 'startYear')
                                    <input type="{{ $param->getType() }}" name="{{ $param->getName() }}" class="form-control {{ $param->getName() }} {{ $errors->has($param->getName())?'form-control-danger':'' }}" min="2006" max="{{ date('Y') }}" value="{{ $param->getValue() }}" required >
                                @else
                                    <input type="{{ $param->getType() }}" name="{{ $param->getName() }}" class="form-control {{ $param->getName() }} {{ $errors->has($param->getName())?'form-control-danger':'' }}" required placeholder="{{ $param->getPlaceHolder() }}" value="{{ $param->getValue() }}">
                                @endif
                                @if($errors->has($param->getName()))
                                    @foreach ($errors->get($param->getName()) as $error)
                                        <div class="form-control-feedback">{{ $error }}</div>
                                    @endforeach
                                @endif
                            @if($param->getType() != 'hidden')
                            </div>
                            @endif
                        @endforeach
                            @if(!Session::has('operator_regnum'))
                                <input type="hidden" name="operator_regnum" required>
                            @endif
                            @if(!Session::has('operator_fingerprint'))
                                <input type="hidden" name="operator_fingerprint" required>
                            @endif
                            {{--<input type="hidden" name="citizen_regnum">--}}
                            <input type="hidden" name="citizen_fingerprint" required>

                    </form>
                </div>
            </div>
            <div class="card border-info">
                <div class="card-header bg-info">
                    <h4 class="m-b-0 text-white">Баталгаажуулах</h4>
                </div>
                @if($authRequired)
                <div class="card-body">
                    <div class="form-group">
                        <div class="row justify-content-md-center">
                            @if(!Session::has('operator_fingerprint'))
                                <div class="col-md-6">
                                    <p class="font-weight-bold">Ажилтны хурууны хээ :</p>
                                    <div class="col-md-8" style="  margin-left: auto; margin-right: auto;">
                                        <button type="button" id="finger-operator" class="finger-print">
                                            <img id="finger-img-operator" src="/images/fp.png">
                                        </button>
                                    </div>
                                </div>
                            @endif
                           <div class="col-md-6" >
                               <p class="font-weight-bold">Иргэний хурууны хээ :</label>
                               <div class="col-md-8" style="  margin-left: auto; margin-right: auto;">
                               <button type="button" id="finger-citizen" class="finger-print">
                                   <img id="finger-img-citizen" src="/images/fp.png">
                               </button>
                               </div>
                           </div>
                        </div>
                    </div>
                </div>
                @endif
                <div class="card-footer">
                    <button id="form-submt" class="btn btn-info float-right ">Хүсэлт илгээх</button>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
@push('styles')
    <style>
        .finger-print {
            padding: 0;
            width: 100%;
        }

        .finger-print img{
            width: 100%;
        }
    </style>
@endpush
@push('scripts')
    <script src="/dist/js/jquery.maskedinput.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
           $(function(){
               // $(".regnum").mask("cc99999999");
               // $(".plateNumber").mask("9999ccc");
               // $(".propertyNumber").mask("c9999999999");
               //
               // $(".regnum").first().focus();
               // $(".plateNumber").first().focus();
               // $(".propertyNumber").first().focus();
           });
        });
        $(function() {
            $(".preloader").fadeOut();
        });
        $(function() {
            $('[data-toggle="tooltip"]').tooltip()
        });

    </script>

    <script>
        handler = {
            info : {
                OFT : function(selector){
                    $(selector).attr('src', '/images/fp-1.gif');
                },
                OFG : function(selector){
                    $(selector).attr('src', '/images/fp.png');
                }
            }
        };

        var Client = function(){
            var self = this;
            self.ws = null;
            self.selector = "";
            self.fp = "";

            self.setSelector = function(selector){
                self.selector = selector;
            };

            self.init = function(){
                self.ws = new WebSocket("ws://localhost:59002");

                self.ws.onopen = function () {
                    console.log('[*] Connection opened %.');
                };

                self.ws.onmessage = function (evt) {
                    if (evt.data.length > 1024) {
                        self.fp += evt.data;
                        return;
                    }
                    try {
                        var response = JSON.parse(evt.data);
                        handler[response.status][response.type](self.selector);
                    } catch (e) {
                        console.log(e);
                        flash_error("Алдаа гарлаа");
                    }
                };

                self.ws.onclose = function (evt) {
                    if (self.fp) {
                        try {
                            console.log(self.fp);
                            var fp = JSON.parse(self.fp);
                            $(self.selector).attr('src', '/images/fp-2.gif');

                            if ('#finger-img-citizen' === self.selector) {
                                $('input[name=citizen_fingerprint]').val(fp.fingerprint);
                            } else {
                                $('input[name=operator_fingerprint]').val(fp.fingerprint);
                            }
                        } catch (e) {
                            console.log(e);
                            flash_error("Хурууны хээг уншихад алдааг гарлаа.");
                        }
                        self.fp = "";
                    } else {
                        $(self.selector).attr('src', '/images/fp.png');
                    }
                    console.log("[*] Connection closed %.");
                    self.init();
                };
            };

            self.init();
        };

        $(function(){
            var client = new Client();
            $('#finger-citizen').on('click', function(){
                $(this).css('border', '5px solid #21546f');
                $('#finger-operator').css('border', '1px solid black');
                client.setSelector('#finger-img-citizen');
            });

            $('#finger-operator').on('click', function(){
                $(this).css('border', '5px solid #21546f');
                $('#finger-citizen').css('border', '1px solid black');
                client.setSelector('#finger-img-operator');
            });

        });
        @if(!$authRequired)
        $("#form-submt").click(function() {
            $('form').submit();
        });
        @else
        $("#form-submt").click(function(){
            if($('input[name=operator_fingerprint]').length ){
                if(!$('input[name=citizen_fingerprint]').val()){
                    $('#finger-citizen').css('border', '5px solid red');
                }
                if(!$('input[name=operator_fingerprint]').val()){
                    $('#finger-operator').css('border', '5px solid red');
                }
                if($('input[name=citizen_fingerprint]').val() && $('input[name=operator_fingerprint]').val()){
                    $('form').submit();
                }
            }else{
                if(!$('input[name=citizen_fingerprint]').val()){
                    $('#finger-citizen').css('border', '5px solid red');
                }else{
                    $('form').submit();
                }
            }
        });
        @endif
    </script>
    @endpush
