<div class="row">
    <h4 class="col-md-12 mb-3">
        Тээврийн хэрэгслийн эзэмшигчийн түүхчилсэн мэдээлэл
    </h4>
    @php
    $list = [];
    if(gettype($response->list) == 'object')
    $list[] = $response->list;
    else
    $list = $response->list;
    @endphp
    @foreach($list as $data )
        <div class="col-md-12 mt-3">
            <table class="table table-striped table-sm">
                <tbody>

                <tr>
                    <th class="text-left">Арлын дугаар:</th>
                    <th class="text-right font-weight-bold">{{ $data->cabinNumber }}</th>
                </tr>
                <tr>
                    <td class="text-left">Эзэмшсэн хугацаа:</td>
                    <td class="text-right font-weight-bold">{{ isset($data->fromDate) ? date('Y-m-d', strtotime($data->fromDate)) : '~'  }} - {{ isset($data->toDate) ? date('Y-m-d', strtotime($data->toDate)) : '~' }}</td>
                </tr>
                <tr>
                    <td class="text-left">Хаяг:</td>
                    <td class="text-right font-weight-bold">{{ $data->fullAddress }}</td>
                </tr>
                <tr>
                    <td class="text-left">Эзэмшигч:</td>
                    <td class="text-right font-weight-bold">{{ isset($data->ownerLastname) ? $data->ownerLastname : '' }} -н {{ isset($data->ownerFirstname)? $data->ownerFirstname : ''  }}</td>
                </tr>
                <tr>
                    <td class="text-left">Регистер:</td>
                    <td class="text-right font-weight-bold">{{ $data->ownerRegnum }}</td>
                </tr>
                <tr>
                    <td class="text-left">Утас:</td>
                    <td class="text-right font-weight-bold">{{ $data->phone }}</td>
                </tr>
                <tr>
                    <td class="text-left">Улсын дугаа:</td>
                    <td class="text-right font-weight-bold">{{ $data->plateNumber }}</td>
                </tr>

                </tbody>
            </table>
        </div>
    @endforeach

</div>
