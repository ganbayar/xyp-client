<div class="row">
    <h4 class="col-md-12 mb-3">
        Үл хөдлөх хөрөнгийн мэдээлэл
    </h4>

    <div class="col-md-12">
        <table class="table table-striped">
            <tbody>

            <tr>
                <td class="text-left">Үл хөдлөх хөрөнгийн дугаар:</td>
                <td class="text-right font-weight-bold">{{ isset($response->propertyNumber) ? $response->propertyNumber : '' }}</td>
            </tr>
            <tr>
                <td class="text-left">Хаяг:</td>
                <td class="text-right font-weight-bold">{{ isset($response->address) ? $response->address : '' }}</td>
            </tr>
            <tr>
                <td class="text-left">Хэмжээ:</td>
                <td class="text-right font-weight-bold">{{ isset($response->square) ? $response->square : '' }}</td>
            </tr>
            {{--<tr>--}}
                {{--<td class="text-left">Хэмжээ:</td>--}}
                {{--<td class="text-right font-weight-bold">{{ isset($response->square) ? $response->square : '' }}</td>--}}
            {{--</tr>--}}
            <tr>
                <td class="text-left">Зориулалт:</td>
                <td class="text-right font-weight-bold">{{ isset($response->intent) ? $response->intent : '' }}</td>
            </tr>

            </tbody>
        </table>
        <table class="table table-bordered table-striped table-sm mt-5">
            <tr>
                <th >Дугаар</th>
                <th >Үйлчилгээ</th>
                <th >Огноо</th>
                <th >Өмлөгч/Эрх бүхий байгууллага</th>
            </tr>
            @foreach($response->processList as $data)
                <tr>
                    <td>{{ isset($data->serviceID) ? $data->serviceID : '' }}</td>
                    <td>{{ isset($data->serviceName) ? $data->serviceName : ''  }}</td>
                    <td class="date-wrap">{{ isset($data->date) ? $data->date : '' }}</td>
                    @if(isset($data->ownerDataLlist))
                        <td>
                            @if( isset($data->ownerDataLlist->firstname) )
                                {{ isset($data->ownerDataLlist->lastname) ? $data->ownerDataLlist->lastname : '' }} {{ isset($data->ownerDataLlist->firstname) ? $data->ownerDataLlist->firstname : '' }}
                            @else
                                @foreach ($data->ownerDataLlist as $owner)
                                    {{ isset($owner->lastname) ? $owner->lastname : '' }} {{ isset($owner->firstname) ? $owner->firstname : '' }} @if(!$loop->last)<br>@endif
                                @endforeach
                            @endif
                        </td>
                    @else
                        <td></td>
                    @endif
                </tr>
            @endforeach
        </table>
    </div>
</div>
