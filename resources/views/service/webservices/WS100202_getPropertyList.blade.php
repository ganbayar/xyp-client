<div class="row">
    <h4 class="col-md-12 mb-3">
        Үл хөдлөх хөрөнгийн жагсаалт
    </h4>
</div>
    @php
        $list = [];
       if(gettype($response->listData) == 'object')
            $list[] = $response->listData;
       else
            $list = $response->listData;

        $is_owner = false;
        $is_nonOwner = false;
        $name = '';
        $register = '';
        foreach ($list as $data){
            if ($data->ownershipId === '000001')
                $is_owner = true;
            else {
                $is_nonOwner = true;
            }
            $name = $data->personNameCorp;
            $register = $data->personCorpId;
        }
        $i = 0;
        $j = 0;
    @endphp
@if($is_owner)
    <div class="row">
        <h5 class="col-md-12 mb-3">
            {{ mb_strtoupper($register) }} регистрийн дугаартай иргэн {{ mb_strtoupper($name) }} -н өмчлөлд дараах эд хөрөнгө бүртгэлтэй байна.
        </h5>
    </div>
<div class="row">
    <div class="col-md-12 mt-3 service-legal">
        <table class="table table-bordered table-sm">
            <tbody>
            <tr>
                <th rowspan="2">№</th>
                <th rowspan="2">Улсын бүртгэлийн дугаар</th>
                <th rowspan="2">Хаяг</th>
                <th rowspan="2">Зориулалт</th>
                <th rowspan="2">Талбайн хэмжээ</th>
                <th rowspan="2">Хөрөнгийн төрөл</th>
                <th rowspan="2">Эрхийн төрөл</th>
                <th colspan="2">Өмчлөгчөөр бүртгүүлсэн</th>
                <th rowspan="2">Өмчлөгчин тоо</th>
            </tr>
            <tr>
                <th>Огноо</th>
                <th>Үндэслэл</th>
            </tr>

                @foreach($list as $data )
                    @if($data->ownershipId === '000001')
                    <tr>
                        <td>{{ ++$i }}</td>
                        <td>

                            <form action="/service/input" method="post">
                                {{ csrf_field() }}
                                <input type="hidden" name="regnum" value="{{ $regnum }}">
                                <input type="hidden" name="propertyNumber" value="{{ $data->propertyNationRegisterNumber }}">
                                <input type="hidden" name="ws_code" value="WS100201_getPropertyInfo">
                                <input type="hidden" name="isReDirect" value="1">
                                <button type="submit" class="btn btn-link">{{ isset($data->propertyNationRegisterNumber) ? $data->propertyNationRegisterNumber : '' }}</button>
                            </form>


                        </td>
                        <td>{{ isset($data->fullAddress) ? $data->fullAddress : '' }}</td>
                        <td>{{ isset($data->codeName) ? $data->codeName : '' }}</td>
                        <td>{{ isset($data->propertySize) ? $data->propertySize : '' }}</td>
                        <td>{{ isset($data->serviceType) ? $data->serviceType : '' }}</td>
                        <td>{{ isset($data->ownershipStatus) ? $data->ownershipStatus : '' }}</td>
                        <td>{{ isset($data->serviceDate) ? date('Y-m-d', strtotime($data->serviceDate)) : '' }}</td>
                        <td>{{ isset($data->serviceName) ? $data->serviceName : '' }}</td>
                        <td>{{ isset($data->movedCount) ? $data->movedCount : '' }}</td>
                    </tr>
                    @endif
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endif
@if($is_nonOwner)
    <div class="row">
        <h5 class="col-md-12 mb-3">
            {{ mb_strtoupper($register) }} регистрийн дугаартай иргэн {{ mb_strtoupper($name) }} -н өмчлөлд дараах эд хөрөнгө бүртгэлтэй байсныг шилжүүлсэн байна.
        </h5>
    </div>
<div class="row">
    <div class="col-md-12 mt-3 service-legal">
        <table class="table table-bordered table-sm">
            <tbody>
            <tr>
                <th rowspan="2">№</th>
                <th rowspan="2">Улсын бүртгэлийн дугаар</th>
                <th rowspan="2">Хаяг</th>
                <th rowspan="2">Зориулалт</th>
                <th rowspan="2">Талбайн хэмжээ</th>
                <th rowspan="2">Хөрөнгийн төрөл</th>
                <th rowspan="2">Эрхийн төрөл</th>
                <th colspan="2">Өмчлөгчөөр бүртгүүлсэн</th>
                <th rowspan="2">Өмчлөгчин тоо</th>
                <th colspan="2">Шилжүүлсэн</th>
            </tr>
            <tr>
                <th>Огноо</th>
                <th>Үндэслэл</th>
                <th>Огноо</th>
                <th>Үндэслэл</th>
            </tr>
            @foreach($list as $data )
                @if($data->ownershipId !== '000001')
                    <tr>
                        <td>{{ ++$j }}</td>
                        <td>{{ isset($data->propertyNationRegisterNumber) ? $data->propertyNationRegisterNumber : '' }}</td>
                        <td>{{ isset($data->fullAddress) ? $data->fullAddress : '' }}</td>
                        <td>{{ isset($data->codeName) ? $data->codeName : '' }}</td>
                        <td>{{ isset($data->propertySize) ? $data->propertySize : '' }}</td>
                        <td>{{ isset($data->serviceType) ? $data->serviceType : '' }}</td>
                        <td>{{ isset($data->ownershipStatus) ? $data->ownershipStatus : '' }}</td>
                        <td>{{ isset($data->serviceDate) ? date('Y-m-d', strtotime($data->serviceDate)) : '' }}</td>
                        <td>{{ isset($data->serviceName) ? $data->serviceName : '' }}</td>
                        <td>{{ isset($data->movedCount) ? $data->movedCount : '' }}</td>
                        <td>{{ isset($data->movedDate) ? date('Y-m-d', strtotime($data->movedDate)) : '' }}</td>
                        <td>{{ isset($data->ownershipStatus) ? $data->ownershipStatus : '' }}</td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>
</div>
@endif
