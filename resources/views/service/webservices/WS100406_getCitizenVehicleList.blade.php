<div class="row">
    <h4 class="col-md-12 text-left mb-3">
        Тээврийн хэрэгслийн мэдээлэл
    </h4>

    <div class="col-md-12">
        <table class="table table-bordered">
            <tbody>
            @php
                $list = [];
                if(gettype($response->list) == 'object')
                    $list[] = $response->list;
                else
                    $list = $response->list;

            @endphp
            <tr>
                <th>№</th>
                <th>Марк</th>
                <th>Үйлдвэрлэсэн он</th>
                <th>Улсын дугаар</th>
                <th>Мэдээлэл</th>
            </tr>
            @foreach($list as $data)
                <tr>
                    <td>
                        {{ $loop->iteration }}
                    </td>
                    <td>
                        {{ $data->markName }} {{ $data->modelName }}
                    </td>
                    <td>
                        {{ $data->buildYear }}
                    </td>
                    <td>
                        {{ $data->plateNumber }}
                    </td>
                    <td>
                        <form action="/service/input" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="regnum" value="{{ $regnum }}">
                            <input type="hidden" name="plateNumber" value="{{ $data->plateNumber }}">
                            <input type="hidden" name="ws_code" value="WS100401_getVehicleInfo">
                            <input type="hidden" name="isReDirect" value="1">
                            <button type="submit" class="btn btn-link">Мэдээлэл авах</button>
                        </form>
                    </td>
                </tr>

                {{--<tr>--}}
                    {{--<th class="text-left">Тээврийн хэрэгсэл:</th>--}}
                    {{--<th class="text-right font-weight-bold">{{ $data->markName }} {{ $data->modelName }} ---}}

                        {{----}}
                    {{--</th>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                    {{--<td class="text-left">Үйлдвэрлэсэн он:</td>--}}
                    {{--<td class="text-right font-weight-bold">{{ $data->buildYear }}</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                    {{--<td class="text-left">Арлын дугаар:</td>--}}
                    {{--<td class="text-right font-weight-bold">{{ $data->cabinNumber }}</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                    {{--<td class="text-left">Хөдөлгүүрийн багтаамж:</td>--}}
                    {{--<td class="text-right font-weight-bold">{{ $data->capacity }}</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                    {{--<td class="text-left">Ангилал:</td>--}}
                    {{--<td class="text-right font-weight-bold">{{ $data->className }}</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                    {{--<td class="text-left">Өнгө:</td>--}}
                    {{--<td class="text-right font-weight-bold">{{ $data->colorName }}</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                    {{--<td class="text-left">Улс:</td>--}}
                    {{--<td class="text-right font-weight-bold">{{ $data->countryName }}</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                    {{--<td class="text-left">Мотрын төрөл:</td>--}}
                    {{--<td class="text-right font-weight-bold">{{ $data->fueltype }}</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                    {{--<td class="text-left">Оруулж ирсэн огноо:</td>--}}
                    {{--<td class="text-right font-weight-bold">{{ date('Y-m-d', strtotime($data->importDate))  }}</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                    {{--<td class="text-left">Суудлын тоо:</td>--}}
                    {{--<td class="text-right font-weight-bold">{{ $data->manCount  }}</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                    {{--<td class="text-left">Дамжуулалт:</td>--}}
                    {{--<td class="text-right font-weight-bold">{{ $data->transmission  }}</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                    {{--<td class="text-left">Төрөл:</td>--}}
                    {{--<td class="text-right font-weight-bold">{{ $data->type  }}</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                    {{--<td class="text-left">Хүрдний байрлал:</td>--}}
                    {{--<td class="text-right font-weight-bold">{{ $data->wheelPosition  }}</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                    {{--<td class="text-left">Жин:</td>--}}
                    {{--<td class="text-right font-weight-bold">{{ $data->mass  }}</td>--}}
                {{--</tr>--}}
            @endforeach
            </tbody>
        </table>
    </div>
</div>
