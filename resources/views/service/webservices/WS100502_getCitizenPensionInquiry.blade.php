<div class="row">
    <div class="col-md-12">
        @if( $response->pensioner)
            <p><i>{{ $request->regnum }}</i> регистрийн дугаартай иргэн нь тэтгэвэрт <b>гарсан</b> болно</p>
        @else
            <p>{{ $request->regnum }} регистрийн дугаартай иргэн нь тэтгэвэрт <b>гараагүй</b> болно</p>
        @endif
    </div>
</div>