<div class="row">
    <h4 class="col-md-12 mb-3">
        Татвар төлөгчийн мэдээлэл
    </h4>

    <div class="col-md-12">
        <table class="table table-sm table-striped">
            <tbody>

            <tr>
                <th class="text-left">Сервис дуудсан огноо:</th>
                <th class="text-right font-weight-bold">{{ isset($response->callDate) ? $response->callDate : '' }}</th>
            </tr>
            <tr>
                <td class="text-left">Баримт таних дугаар:</td>
                <td class="text-right font-weight-bold">{{ isset($response->callNumber) ? $response->callNumber : '' }}</td>
            </tr>
            <tr>
                <td class="text-left">Байгуулагдсан огноо:</td>
                <td class="text-right font-weight-bold">{{ isset($response->foundDate) ? $response->foundDate : '' }}</td>
            </tr>
            <tr>
                <td class="text-left">Удирдлагын овог, нэр:</td>
                <td class="text-right font-weight-bold">{{ isset($response->headnames) ? $response->headnames : '' }}</td>
            </tr>
            <tr>
                <td class="text-left">Татварын албаны нэр:</td>
                <td class="text-right font-weight-bold">{{ isset($response->officeName) ? $response->officeName : '' }}</td>
            </tr>
            <tr>
                <td class="text-left">Эрхлэх үйл ажиллагааны код:</td>
                <td class="text-right font-weight-bold">{{ isset($response->businessCode) ? $response->businessCode : '' }}</td>
            </tr>
            <tr>
                <td class="text-left">Эрхлэх үйл ажиллагааны нэр:</td>
                <td class="text-right font-weight-bold">{{ isset($response->businessName) ? $response->businessName : '' }}</td>
            </tr>
            <tr>
                <td class="text-left">Хаана зориулж:</td>
                <td class="text-right font-weight-bold">{{ isset($response->dedicateOrganisation) ? $response->dedicateOrganisation : '' }}</td>
            </tr>
            <tr>
                <td class="text-left">Татан буугдсан огноо:</td>
                <td class="text-right font-weight-bold">{{ isset($response->deletedDate) ? $response->deletedDate : '' }}</td>
            </tr>
            <tr>
                <td class="text-left">Татан буугдсан шалтгаан:</td>
                <td class="text-right font-weight-bold">{{ isset($response->deletedReason) ? $response->deletedReason : '' }}</td>
            </tr>
            <tr>
                <td class="text-left">Бүртгэлийн дугаар:</td>
                <td class="text-right font-weight-bold">{{ isset($response->id) ? $response->id : '' }}</td>
            </tr>
            <tr>
                <td class="text-left">Хариуцлагын хэлбэрийн код:</td>
                <td class="text-right font-weight-bold">{{ isset($response->legalStatusCode) ? $response->legalStatusCode : '' }}</td>
            </tr>
            <tr>
                <td class="text-left">Хариуцлагын хэлбэрийн нэр:</td>
                <td class="text-right font-weight-bold">{{ isset($response->legalStatusName) ? $response->legalStatusName : '' }}</td>
            </tr>
            <tr>
                <td class="text-left">Дуудаж байгаа газар:</td>
                <td class="text-right font-weight-bold">{{ isset($response->organisationId) ? $response->organisationId : '' }}</td>
            </tr>
            <tr>
                <td class="text-left">Баримт таних дугаар:</td>
                <td class="text-right font-weight-bold">{{ isset($response->phoneNumber) ? $response->phoneNumber : '' }}</td>
            </tr>
            <tr>
                <td class="text-left">Зориулалт:</td>
                <td class="text-right font-weight-bold">{{ isset($response->dedication) ? $response->dedication : '' }}</td>
            </tr>
            <tr>
                <td class="text-left">Нэр:</td>
                <td class="text-right font-weight-bold">{{ isset($response->name) ? $response->name : '' }}</td>
            </tr>
            <tr>
                <td class="text-left">Иргэний оршин суугаа хаяг:</td>
                <td class="text-right font-weight-bold">{{ isset($response->address) ? $response->address : '' }}</td>
            </tr>
            <tr>
                <td class="text-left">Албан бичгийн хариу:</td>
                <td class="text-right font-weight-bold">{{ isset($response->documentResponse) ? $response->documentResponse : '' }}</td>
            </tr>
            <tr>
                <td class="text-left">Татварын албаны код:</td>
                <td class="text-right font-weight-bold">{{ isset($response->officeCode) ? $response->officeCode : '' }}</td>
            </tr>

            </tbody>
        </table>
    </div>
</div>
