<div class="row">
    <div class="col-md-12">
        <table class="table table-condensed table-striped">
            <thead>
                <td>Овог</td>
                <td>Нэр</td>
                <td>Төрсөн огноо</td>
                <td>Улс төрд нөлөө бүхий этгээд</td>
                <td>Олон улсын хоригт орсон эсэх</td>
                <td>Оффшор бүсэд бүртгэлтэй, данстай эсэх</td>
                <td>Олон улсад эрэн сурвалжлагдаж байгаа эсэх</td>
            </thead>
            <tbody>
                <tr>
                    <td>Ганболд</td>
                    <td>Ганзориг</td>
                    <td>1988 он 10 сар</td>
                    <td>Тийм</td>
                    <td>Үгүй</td>
                    <td>Тийм</td>
                    <td>Үгүй</td>
                </tr>
                <tr>
                    <td>Ганболд</td>
                    <td>Ганзориг</td>
                    <td>1988 он 10 сар</td>
                    <td>Тийм</td>
                    <td>Үгүй</td>
                    <td>Тийм</td>
                    <td>Үгүй</td>
                </tr>
                <tr>
                    <td>Ганболд</td>
                    <td>Ганзориг</td>
                    <td>1988 он 10 сар</td>
                    <td>Тийм</td>
                    <td>Үгүй</td>
                    <td>Тийм</td>
                    <td>Үгүй</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
