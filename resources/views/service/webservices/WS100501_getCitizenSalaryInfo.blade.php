<div class="row">
    <h4 class="col-md-12 mb-3">
        Нийгмийн даатгалын мэдээлэл
    </h4>

    <div class="col-md-12">
        <table class="table table-nd table-striped">
            <thead>
            <tr>
                <th rowspan="2">Хэлтэсийн нэр</th>
                <th rowspan="2">Ажил олгогчийн нэр</th>
                <th rowspan="2">Он</th>
                <th rowspan="2">Сар</th>
                <th colspan="2">Даатгуулагч</th>
                <th rowspan="2">Ажил олгогч шимтгэл төлсөн эсэх</th>
            </tr>
            <tr>
                <th>Цалин</th>
                <th>НДШ</th>
            </tr>
            </thead>

            <tbody>

            @foreach($response->list as $data)
                <tr>
                    <td>
                        @if(isset($data->domName))
                            {{ $data->domName }}
                        @endif
                    </td>
                    <td>
                        @if(isset($data->orgName))
                            {{ $data->orgName }}
                        @endif
                    </td>
                    <td>
                        @if(isset($data->year))
                            {{ $data->year }}
                        @endif
                    </td>
                    <td>
                        @if(isset($data->month))
                            {{ $data->month }}
                        @endif
                    </td>
                    <td>
                        @if(isset($data->salaryAmount))
                            {{ $data->salaryAmount }}
                        @endif
                    </td>
                    <td>
                        @if(isset($data->salaryFee))
                            {{ $data->salaryFee }}
                        @endif
                    </td>
                    <td>
                        @if($data->paid)
                            Төлсөн
                        @else
                            Төлөөгүй
                        @endif
                    </td>

                </tr>
            @endforeach


            </tbody>
        </table>
    </div>
</div>
