<div class="row">
    <h4 class="col-md-12 mb-3">
        Гэрлэлт цуцлалтын мэдээлэл
    </h4>

    <div class="col-md-12">
        <table class="table table-striped">
            <tbody>
            <tr>
                <td class="text-left">Цуцлуулсан аймаг, хот:</td>
                <td class="text-right font-weight-bold" colspan="2">{{ $response->cancelCity }}</td>
            </tr>
            <tr>
                <td class="text-left">Цуцлуулсан сум, дүүрэг:</td>
                <td class="text-right font-weight-bold" colspan="2">{{$response->cancelDistrict }}</td>
            </tr>
            <tr>
                <td class="text-left">Цуцлуулсан огноо:</td>
                <td class="text-right font-weight-bold" colspan="2">{{ date('Y-m-d', strtotime($response->cancelDate)) }}</td>
            </tr>
            <tr>
                <td class="text-left">Цуцлуулсан дугаар:</td>
                <td class="text-right font-weight-bold" colspan="2">{{ $response->cancelId }}</td>
            </tr>
            <tr>
                <td class="text-left">Батлуулсан аймаг, хот:</td>
                <td class="text-right font-weight-bold" colspan="2">{{ $response->courtCity }}</td>
            </tr>
            <tr>
                <td class="text-left">Батлуулсан сум, дүүрэг:</td>
                <td class="text-right font-weight-bold" colspan="2">{{$response->courtDistrict }}</td>
            </tr>
            <tr>
                <td class="text-left">Батлуулсан огноо:</td>
                <td class="text-right font-weight-bold" colspan="2">{{ date('Y-m-d', strtotime($response->courtDate)) }}</td>
            </tr>
            <tr>
                <td class="text-left">Батлуулсан  дугаар:</td>
                <td class="text-right font-weight-bold" colspan="2">{{ $response->courtId }}</td>
            </tr>
            <tr>
                <td class="text-left">Гэрлэсэн огноо:</td>
                <td class="text-right font-weight-bold" colspan="2">{{ date('Y-m-d', strtotime($response->marriedDate)) }}</td>
            </tr>


            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <h5 class="col-md-12 text-left mb-3">
        Нөхөр
    </h5>
    <div class="col-md-12">
        <table class="table table-striped">
            <tbody>

            <tr>
                <td class="text-left">Овог:</td>
                <td class="text-right font-weight-bold">{{ $response->husband->lastname }}</td>
            </tr>
            <tr>
                <td class="text-left">Нэр:</td>
                <td class="text-right font-weight-bold">{{ $response->husband->firstname }}</td>
            </tr>
            <tr>
                <td class="text-left">Регистер:</td>
                <td class="text-right font-weight-bold">{{ $response->husband->regnum }}</td>
            </tr>
            {{--<tr>--}}
                {{--<td class="text-left">Өмнөх төлөв:</td>--}}
                {{--<td class="text-right font-weight-bold">{{ $response->wife->marriageStatus }}</td>--}}
            {{--</tr>--}}
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <h5 class="col-md-12 text-left mb-3">
        Эхнэр
    </h5>
    <div class="col-md-12">
        <table class="table table-striped">
            <tbody>

            <tr>
                <td class="text-left">Овог:</td>
                <td class="text-right font-weight-bold">{{ $response->wife->lastname }}</td>
            </tr>
            <tr>
                <td class="text-left">Нэр:</td>
                <td class="text-right font-weight-bold">{{ $response->wife->firstname }}</td>
            </tr>
            <tr>
                <td class="text-left">Регистер:</td>
                <td class="text-right font-weight-bold">{{ $response->wife->regnum }}</td>
            </tr>
            {{--<tr>--}}
                {{--<td class="text-left">Өмнөх төлөв:</td>--}}
                {{--<td class="text-right font-weight-bold">{{ $response->wife->marriageStatus }}</td>--}}
            {{--</tr>--}}
            </tbody>
        </table>
    </div>
</div>

