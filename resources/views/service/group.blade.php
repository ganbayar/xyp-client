@extends('layouts.master')
@section('content')
    <div class="toggler">
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor" style="padding: 0.375rem 0.75rem;">Үйлчилгээ</h4>
            </div>

        </div>

        <div class="card border-info">
            <div class="card-header bg-info">
                <h4 class="m-b-0 text-white">Үйлчилгээ сонгох</h4>
            </div>
            <div class="card-body">
                <div class="row">
                    @foreach($groups as $group)
                        <div class="col-lg-3 col-md-3">
                            <div class="card text-white bg-info">
                                <div class="card-body">
                                    <h4 class="card-title text-white">{{ $group->title }} <a class="get-code collapsed" data-toggle="collapse" href="#tt-{{$group->id}}" aria-expanded="true"><i class="fas fa-info-circle" title="Дэлгэрэнгүй" data-toggle="tooltip"></i></a></h4>
                                    <div class="collapse m-t-15" id="tt-{{$group->id}}" aria-expanded="true">
                                        <ul class="list-group">
                                            @foreach($group->services as $service)
                                                <li class="list-group-item" style="color:#2f7396;" data-toggle="tooltip" data-placement="top" title="{{ $service->ws_code }}"> {{ $service->title }} </li>

                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-footer text-center group-footer" >
                                    <form method="post">
                                        {{ csrf_field() }}
                                        <input type="hidden" value="{{ $group->id }}" name="service_group_id">
                                        <button  type="submit" class="btn btn-rounded btn-choose" > сонгох</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="card border-info">
            <div class="card-header bg-info">
                <h4 class="m-b-0 text-white">Үйлчилгээ үүсгэх</h4>
            </div>
            <div class="card-body">
                <form class="form-material" action="" method="post">
                    {{ csrf_field() }}
                    <h4 class="card-title">Иргэн :</h4>
                    <div class="input-group">
                    @foreach($webservices as $webservice)
                            @if(strpos($webservice->ws_code,'1001'))
                                <div class="col-lg-3 col-md-3 m-b-20">
                                    <input  name="service_set[]" type="checkbox" value="{{$webservice->ws_code}}" class="check form-group m-l-15" id="minimal-checkbox-{{ $loop->index+1 }}" data-checkbox="icheckbox_square-red">
                                    <label class="icheck-label" for="minimal-checkbox-{{ $loop->index+1 }}" data-toggle="tooltip" data-placement="top" title="{{ $webservice->ws_code }}">
                                        {{ $webservice->title }}
                                    </label>
                                </div>
                            @endif
                        @endforeach
                    </div>
                    <hr>
                    <h4 class="card-title">Үл хөдлөх хөрөнгө :</h4>
                    <div class="input-group">
                        @foreach($webservices as $webservice)
                            @if(strpos($webservice->ws_code,'1002'))
                                <div class="col-lg-3 col-md-3 m-b-20">
                                    <input  name="service_set[]" type="checkbox" value="{{$webservice->ws_code}}" class="check form-group m-l-15" id="minimal-checkbox-{{ $loop->index+1 }}" data-checkbox="icheckbox_square-red">
                                    <label class="icheck-label" for="minimal-checkbox-{{ $loop->index+1 }}" data-toggle="tooltip" data-placement="top" title="{{ $webservice->ws_code }}">
                                        {{ $webservice->title }}
                                    </label>
                                </div>
                            @endif
                        @endforeach
                    </div>
                    <hr>
                    <h4 class="card-title">Хуулийн этгээд :</h4>
                    <div class="input-group">
                        @foreach($webservices as $webservice)
                            @if(strpos($webservice->ws_code,'1003'))
                                <div class="col-lg-3 col-md-3 m-b-20">
                                    <input  name="service_set[]" type="checkbox" value="{{$webservice->ws_code}}" class="check form-group m-l-15" id="minimal-checkbox-{{ $loop->index+1 }}" data-checkbox="icheckbox_square-red">
                                    <label class="icheck-label" for="minimal-checkbox-{{ $loop->index+1 }}" data-toggle="tooltip" data-placement="top" title="{{ $webservice->ws_code }}">
                                        {{ $webservice->title }}
                                    </label>
                                </div>
                            @endif
                        @endforeach
                    </div>
                    <hr>
                    <h4 class="card-title">Тээврийн хэрэгсэл :</h4>
                    <div class="input-group">
                        @foreach($webservices as $webservice)
                            @if(strpos($webservice->ws_code,'1004'))
                                <div class="col-lg-3 col-md-3 m-b-20">
                                    <input  name="service_set[]" type="checkbox" value="{{$webservice->ws_code}}" class="check form-group m-l-15" id="minimal-checkbox-{{ $loop->index+1 }}" data-checkbox="icheckbox_square-red">
                                    <label class="icheck-label" for="minimal-checkbox-{{ $loop->index+1 }}" data-toggle="tooltip" data-placement="top" title="{{ $webservice->ws_code }}">
                                        {{ $webservice->title }}
                                    </label>
                                </div>
                            @endif
                        @endforeach
                    </div>
                    <hr>
                    <h4 class="card-title">Бусад :</h4>
                    <div class="input-group">
                        @foreach($webservices as $webservice)
                            @if(strpos($webservice->ws_code,'1005') || strpos($webservice->ws_code,'1006'))
                                <div class="col-lg-3 col-md-3 m-b-20">
                                    <input  name="service_set[]" type="checkbox" value="{{$webservice->ws_code}}" class="check form-group m-l-15" id="minimal-checkbox-{{ $loop->index+1 }}" data-checkbox="icheckbox_square-red">
                                    <label class="icheck-label" for="minimal-checkbox-{{ $loop->index+1 }}" data-toggle="tooltip" data-placement="top" title="{{ $webservice->ws_code }}">
                                        {{ $webservice->title }}
                                    </label>
                                </div>
                            @endif
                        @endforeach
                    </div>
                    <hr>
                    <div class="row footer_btn">
                        <button class="btn btn-rounded btn-info m-l-15" type="submit"><i class="fa fa-plus-circle"></i> үүсгэх</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('styles')
    <link href="/assets/node_modules/icheck/skins/all.css" rel="stylesheet">
    <link href="dist/css/pages/form-icheck.css" rel="stylesheet">
    <link href="dist/css/pages/stylish-tooltip.css" rel="stylesheet">

    <style>
        .footer_btn{
            float: right;
        }
        .icheckbox_square-red{
            vertical-align: top;
        }
        .icheck-label{
            width: calc(100% - 30px);
            margin-bottom: 0px;
        }
        .group-footer{
            border-top: 1px solid #fff !important;
        }
        .badge-pill{
            margin-left: 0px;
        }
    </style>
@endpush
@push('scripts')
    <script>
        $(window).on("load", function () {
            $("body").trigger("resize");
            $("body").addClass("mini-sidebar");
            $('.navbar-brand span').hide();
        });
    </script>
    <script src="/assets/node_modules/icheck/icheck.min.js"></script>
    <script src="/assets/node_modules/icheck/icheck.init.js"></script>
@endpush