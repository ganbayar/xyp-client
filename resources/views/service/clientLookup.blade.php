@extends('layouts.master')
@section('content')
    <div class="toggler">
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor" style="padding: 0.375rem 0.75rem;">Өндөр эрсдэлтэй харилцагч хайх</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="card border-info">
                    <div class="card-header bg-info">
                        <h4 class="m-b-0 text-white">Харилцагч</h4>
                    </div>
                    <div class="card-body">
                        <form method="post">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="">Овог</label>
                                <input type="text" name="lastname" value="" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Нэр</label>
                                <input type="text" name="firstname" value="" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Төрсөн огноо</label>
                                <input type="date" name="dob" value="" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Иргэншил</label>
                                <select class="form-control" name="nationality">
                                    <option value="mn" selected="true">Монгол</option>
                                    <option value="other">Гадаад</option>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-secondary float-right">Хайх</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="card border-info">
            <div class="card-header bg-info">
                <h4 class="m-b-0 text-white">Үр дүн</h4>
            </div>
            <div class="card-body">
                <table class="table table-condensed table-striped">
                    <thead>
                        <td>Овог</td>
                        <td>Нэр</td>
                        <td>Төрсөн огноо</td>
                        <td>Улс төрд нөлөө бүхий этгээд</td>
                        <td>Олон улсын хоригт орсон эсэх</td>
                        <td>Оффшор бүсэд бүртгэлтэй, данстай эсэх</td>
                        <td>Олон улсад эрэн сурвалжлагдаж байгаа эсэх</td>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Ганболд</td>
                            <td>Ганзориг</td>
                            <td>1988 он 10 сар</td>
                            <td>Тийм</td>
                            <td>Үгүй</td>
                            <td>Тийм</td>
                            <td>Үгүй</td>
                        </tr>
                        <tr>
                            <td>Ганболд</td>
                            <td>Ганзориг</td>
                            <td>1988 он 10 сар</td>
                            <td>Тийм</td>
                            <td>Үгүй</td>
                            <td>Тийм</td>
                            <td>Үгүй</td>
                        </tr>
                        <tr>
                            <td>Ганболд</td>
                            <td>Ганзориг</td>
                            <td>1988 он 10 сар</td>
                            <td>Тийм</td>
                            <td>Үгүй</td>
                            <td>Тийм</td>
                            <td>Үгүй</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
