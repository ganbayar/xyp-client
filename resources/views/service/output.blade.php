@extends('layouts.master')
@section('content')

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Хэрэглэгчийн мэдээлэл</h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <a href="/get-pdf" target="_blank" id="pdf" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Татах</a>
                <button type="button" id="print" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Хэвлэх</button>
            </div>
        </div>
    </div>


    <div class="card">
        <div class="card-body">
            <div class="vtabs customvtab">
                <ul class="nav nav-tabs tabs-vertical" role="tablist">
                    @foreach($service_output as $ws_code => $value)
                        <li class="nav-item"> <a class="nav-link {{ $loop->first ? 'active' : '' }}" data-toggle="tab" href="#{{ $ws_code }}" role="tab"><span class="hidden-sm-up">{{ $value['name'] }}</span> </a> </li>
                    @endforeach

                </ul>
                <!-- Tab panes -->
                <div class="tab-content printableArea">
                    @foreach($service_output as $ws_code => $value)
                        <div class="tab-pane {{ $loop->first ? 'active' : '' }}" id="{{$ws_code}}" role="tabpanel">
                            <div class="">
                                @include('service.webservices.header')
                                @if($value['result']->resultCode == 0)
                                    @if($ws_code == 'WS100202_getPropertyList' or $ws_code == 'WS100406_getCitizenVehicleList')
                                        @include('service.webservices.'. $ws_code, ['response' => $value['result']->response, 'request' => $value['result']->request,'regnum'=>$value['regnum']])
                                    @else
                                        @include('service.webservices.'. $ws_code, ['response' => $value['result']->response, 'request' => $value['result']->request])
                                    @endif
                                @else
                                    @php $message = '' @endphp
                                    @switch($value['result']->resultCode)
                                        @case(1) @php $message = 'Үр дүн олдсонгүй' @endphp @break
                                        @case(2) @php $message = 'Шалгалтын явцад алдаа гарлаа' @endphp @break
                                        @case(3) @php $message = 'Баталгаажуулалт буруу' @endphp @break
                                        @case(301) @php $message = 'Хурууны хээ олдсонгүй' @endphp @break
                                        @case(302) @php $message = 'Мэдээлэл зөрүүтэй байна' @endphp @break
                                        @case(303) @php $message = 'Хурууны хээ тулгах процесс хэт удаан' @endphp @break
                                        @case(304) @php $message = 'Хурууны хээ тулгахад алдаа гарлаа' @endphp @break
                                        @case(402) @php $message = 'Ззэмшигчийн мэдээлэл зөрүүтэй' @endphp @break
                                        @default @php $message = 'Алдаа' @endphp
                                    @endswitch
                                    @include('service.webservices.error', ['title'=>$value['name'],'code' => $value['result']->resultCode, 'message' => $message,'regnum'=>$value['regnum']])
                                @endif
                                @php
                                    $requestId = isset($value['result']->requestId) ? $value['result']->requestId : 0;
                                @endphp
                                @include('service.webservices.footer', ['id' => $requestId ])
                            </div>
                        </div>
                    @endforeach


                </div>
                <div id="editor">

                </div>
            </div>
        </div>
        <div class="card-footer">
        </div>
    </div>
@endsection
@push('styles')
    <link href="/dist/css/pages/tab-page.css" rel="stylesheet">
@endpush
@push('scripts')
    <script src="/dist/js/pages/jquery.PrintArea.js"></script>
<script>

    $(document).ready(function() {
        $("#print").click(function() {
            var mode = 'iframe'; //popup
            var close = mode == "popup";
            var options = {
                mode: mode,
                popClose: close
            };
            $("div.printableArea").printArea(options);
        });
    });

</script>
@endpush
