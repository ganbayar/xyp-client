<div class="row">
    <h4 class="col-md-12 mb-3">
        Оршин суугаа газрын мэдээлэл
    </h4>
</div>
<div class="row">
    <div class="col-md-12">
        <table class="table table-striped">
            <tbody>
            <tr>
                <td class="text-left">Регистер</td>
                <td class="text-right font-weight-bold">{{ $response->regnum }}</td>
            </tr>
            <tr>
                <td class="text-left">Ургийн овог</td>
                <td class="text-right font-weight-bold">{{ $response->surname }}</td>
            </tr>
            <tr>
                <td class="text-left">Овог:</td>
                <td class="text-right font-weight-bold">{{ $response->lastname }}</td>
            </tr>
            <tr>
                <td class="text-left">Нэр</td>
                <td class="text-right font-weight-bold">{{ $response->firstname }}</td>
            </tr>
            <tr>
                <td class="text-left">Хаяг</td>
                <td class="text-right font-weight-bold">{{ $response->fullAddress  }}</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
