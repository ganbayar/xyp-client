<div class="row">
    <h4 class="col-md-12 mb-3">
        Иргэний үнэмлэхний мэдээлэл
    </h4>
</div>
<div class="row">
    <div class="col-md-12 col-xs-12">

        {{--@php--}}
        {{--var_dump($response);--}}
        {{--exit;--}}
        {{--die;--}}
        {{--@endphp--}}
        <img src="data:image/jfif;base64, {{ base64_encode($response->image) }}" class="rounded mx-auto border user-img" width="160">
    </div>
</div>
    <div class="row">
    <div class="col-md-12 col-xs-12">
        <table class="table table-sm table-striped">
            <tbody>
            <tr>
                <td class="text-left">Регистер</td>
                <td class="text-right font-weight-bold">{{ $response->regnum }}</td>
            </tr>
            <tr>
                <td class="text-left">Ургийн овог</td>
                <td class="text-right font-weight-bold">{{ $response->surname }}</td>
            </tr>
            <tr>
                <td class="text-left">Эцэг/эхийн нэр</td>
                <td class="text-right font-weight-bold">{{ $response->lastname }}</td>
            </tr>
            <tr>
                <td class="text-left">Нэр</td>
                <td class="text-right font-weight-bold">{{ $response->firstname }}</td>
            </tr>
            <tr>
                <td class="text-left">Яс үндэс</td>
                <td class="text-right font-weight-bold">{{ $response->nationality }}</td>
            </tr>
            <tr>
                <td class="text-left">Хүйс</td>
                <td class="text-right font-weight-bold">{{ $response->gender }}</td>
            </tr>
            <tr>
                <td class="text-left">Төрсөн он-сар-өдөр</td>
                <td class="text-right font-weight-bold">{{ date('Y-m-d', strtotime($response->birthDateAsText))  }}</td>
            </tr>
            <tr>
                <td class="text-left">Төрсөн газар</td>
                <td class="text-right font-weight-bold">{{ $response->birthPlace }}</td>
            </tr>
            <tr>
                <td class="text-left">Иргэний үнэмлэх дээрхи хаяг</td>
                <td class="text-right font-weight-bold">{{ $response->passportAddress }}</td>
            </tr>
            <tr>
                <td class="text-left">Бүртгэлийн дугаар</td>
                <td class="text-right font-weight-bold">{{ $response->civilId }}</td>
            </tr>
            <tr>
                <td class="text-left">Олгосон огноо</td>
                <td class="text-right font-weight-bold">{{ $response->passportIssueDate }}</td>
            </tr>
            <tr>
                <td class="text-left">Хүчинтэй хугацаа</td>
                <td class="text-right font-weight-bold">{{ $response->passportExpireDate }}</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
