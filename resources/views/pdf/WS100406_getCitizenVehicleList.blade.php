<div class="row">
    <h4 class="col-md-12 mb-3">
        Тээврийн хэрэгслийн мэдээлэл
    </h4>
</div>
<div class="row">
    <div class="col-md-12">
        <table class="table table-striped">
            <tbody>
            @php
                $list = [];
                if(gettype($response->list) == 'object')
                    $list[] = $response->list;
                else
                    $list = $response->list;

            @endphp
            @foreach($list as $data)
                <tr>
                    <th class="text-left">Тээврийн хэрэгсэл:</th>
                    <th class="text-right font-weight-bold">{{ $data->markName }} {{ $data->modelName }} - {{ $data->plateNumber }}</th>
                </tr>
                <tr>
                    <td class="text-left">Үйлдвэрлэсэн он:</td>
                    <td class="text-right font-weight-bold">{{ $data->buildYear }}</td>
                </tr>
                <tr>
                    <td class="text-left">Арлын дугаар:</td>
                    <td class="text-right font-weight-bold">{{ $data->cabinNumber }}</td>
                </tr>
                <tr>
                    <td class="text-left">Хөдөлгүүрийн багтаамж:</td>
                    <td class="text-right font-weight-bold">{{ $data->capacity }}</td>
                </tr>
                <tr>
                    <td class="text-left">Ангилал:</td>
                    <td class="text-right font-weight-bold">{{ $data->className }}</td>
                </tr>
                <tr>
                    <td class="text-left">Өнгө:</td>
                    <td class="text-right font-weight-bold">{{ $data->colorName }}</td>
                </tr>
                <tr>
                    <td class="text-left">Улс:</td>
                    <td class="text-right font-weight-bold">{{ $data->countryName }}</td>
                </tr>
                <tr>
                    <td class="text-left">Мотрын төрөл:</td>
                    <td class="text-right font-weight-bold">{{ $data->fueltype }}</td>
                </tr>
                <tr>
                    <td class="text-left">Оруулж ирсэн огноо:</td>
                    <td class="text-right font-weight-bold">{{ date('Y-m-d', strtotime($data->importDate))  }}</td>
                </tr>
                <tr>
                    <td class="text-left">Суудлын тоо:</td>
                    <td class="text-right font-weight-bold">{{ $data->manCount  }}</td>
                </tr>
                <tr>
                    <td class="text-left">Дамжуулалт:</td>
                    <td class="text-right font-weight-bold">{{ $data->transmission  }}</td>
                </tr>
                <tr>
                    <td class="text-left">Төрөл:</td>
                    <td class="text-right font-weight-bold">{{ $data->type  }}</td>
                </tr>
                <tr>
                    <td class="text-left">Хүрдний байрлал:</td>
                    <td class="text-right font-weight-bold">{{ $data->wheelPosition  }}</td>
                </tr>
                <tr>
                    <td class="text-left">Жин:</td>
                    <td class="text-right font-weight-bold">{{ $data->mass  }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
