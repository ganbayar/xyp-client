<div class="row ">
    <h4 class="col-md-12 mb-3">
        Хуулийн этгээдийн мэдээлэл
    </h4>
</div>
<div class="row ">
    <h5 class="col-md-12 mb-2">
        Үндсэн үзүүлэлт
    </h5>
</div>

<div class="row service-legal">
        <div class="col-md-12">
            <table class="table table-bordered ">
                <tbody>
                    <tr>
                        <th colspan="4">Улсын бүртгэлд бүртгэсэн</th>
                        <th rowspan="2">Улсын бүртгэ -лийн дугаар</th>
                        <th rowspan="2">Хуулийн этгээдийн төрөл</th>
                        <th rowspan="2">Хуулийн этгээдийн хэлбэр</th>
                        <th rowspan="2">Хуулийн этгээдийг байгуулсан арга зам</th>
                        <th rowspan="2">Хуулийн этгээдийн өмчийн хэлбэр</th>
                        <th rowspan="2">Үүсгэн байгуулсан гишүүний тоо</th>
                        <th colspan="5">Үүсгэн байгуулах</th>
                    </tr>
                    <tr>
                        <th>Байгуул -лагын нэр</th>
                        <th>Шийдвэ -рийн дугаар</th>
                        <th>Бүртгэ -сэн огноо</th>
                        <th>Бүртгэ -сэн огноо</th>
                        <th>Баримт бичгийн нэр</th>
                        <th>Шийдвэ -рийн нэр</th>
                        <th>Шийдвэ -рийн дугаар</th>
                        <th>Дүрмийн огноо</th>
                        <th>Гэрээний огноо</th>
                    </tr>
                    <tr>
                        <td>{{ isset($response->general->companyTitle) ? $response->general->companyTitle : '' }}</td>
                        <td>{{ isset($response->general->regInsttNo) ? $response->general->regInsttNo : '' }}</td>
                        <td>{{ isset($response->general->regInsttDate) ? $response->general->regInsttDate : '' }}</td>
                        <td>{{ isset($response->general->regDate) ? $response->general->regDate : '' }}</td>
                        <td>{{ isset($response->general->nationRegNo) ? $response->general->nationRegNo : '' }}</td>
                        <td>{{ isset($response->general->type) ? $response->general->type : '' }}</td>
                        <td>{{ isset($response->general->form) ? $response->general->form : '' }}</td>
                        <td>{{ isset($response->general->foundationWay) ? $response->general->foundationWay : '' }}</td>
                        <td>{{ isset($response->general->ownershipForm) ? $response->general->ownershipForm : '' }}</td>
                        <td>{{ isset($response->general->foundationMemberCount) ? $response->general->foundationMemberCount : '' }}</td>
                        <td>{{ isset($response->general->foundationDocName) ? $response->general->foundationDocName : '' }}</td>
                        <td>{{ isset($response->general->foundationDecName) ? $response->general->foundationDecName : '' }}</td>
                        <td>{{ isset($response->general->foundationDecNo) ? $response->general->foundationDecNo : '' }}</td>
                        <td>{{ isset($response->general->foundationRegulationDate) ? $response->general->foundationRegulationDate : '' }}</td>
                        <td>
                            {{--{{ isset($response->general->foundationContractDate)  }}--}}
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
</div>
<div class="row ">
    <h5 class="col-md-12 mb-2">
        Хаягийн тухай мэдээлэл
    </h5>
</div>
<div class="row service-legal">
    <div class="col-md-12">
        <table class="table table-bordered">
            <tbody>
            <tr>
                <th rowspan="2">Хаягийн байдал</th>
                <th rowspan="2">Бүртгэсэн огноо</th>
                <th rowspan="2">Байнгын эсэх</th>
                <th colspan="4">Байршил</th>


            </tr>
            <tr>
                <th>Хаяг</th>
                <th>Утасны дугаар</th>
                <th>Факс</th>
                <th>И-мэйл хаяг</th>
            </tr>
            @php
            $list = [];
            if(gettype($response->address) == 'object')
            $list[] = $response->address;
            else
            $list = $response->address;
            @endphp
            @foreach($list as $data )
                <tr>
                    <td>{{ isset($data->state) ? $data->state : '' }}</td>
                    <td>{{ isset($data->regDate) ? $data->regDate : '' }}</td>
                    <td>{{ isset($data->isPermanent) ? $data->isPermanent : '' }}</td>
                    <td>{{ isset($data->address_detail) ? $data->address_detail : '' }}</td>
                    <td>{{ isset($data->phone1) ? $data->phone1 : '' }}</td>
                    <td>
                        {{ isset($data->fax) ? $data->fax : '' }}
                    </td>
                    <td>
                        {{ isset($data->email) ? $data->email : ''}}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
<div class="row ">
    <h5 class="col-md-12 mb-2">
        Үүсгэн байгуулагчийн тухай мэдээлэл
    </h5>
</div>
<div class="row service-legal">
    <div class="col-md-12">
        <table class="table table-bordered ">
            <tbody>
            <tr>
                <th rowspan="2">Одоогийн эсэх</th>
                <th rowspan="2">Бүртгэсэн огноо</th>
                <th colspan="9">Үүсгэн байгуулач</th>
            </tr>
            <tr>
                <th>Регистер</th>
                <th>Эцэг/эхийн нэр</th>
                <th>Өөрийн нэр</th>
                <th>Улсын нэр</th>
                <th>Өмчийн хэлбэр</th>
                <th>Хаяг</th>
                <th>Утасны дугаар</th>
                <th>Оруулсан хөрөнгө</th>
                <th>Хувь</th>
            </tr>
            @php
                $list = [];
                if(gettype($response->founder) == 'object')
                $list[] = $response->founder;
                else
                $list = $response->founder;
            @endphp
            @foreach($list as $data )
                <tr>
                    <td>{{ isset($data->isCurrent) ? $data->isCurrent : '' }}</td>
                    <td>{{ isset($data->regDate) ? $data->regDate : '' }}</td>
                    <td>{{ isset($data->register) ? $data->register : '' }}</td>
                    <td>{{ isset($data->lastname) ? $data->lastname : '' }}</td>
                    <td>{{ isset($data->firstname) ? $data->firstname : '' }}</td>
                    <td>{{ isset($data->country) ? $data->country : '' }}</td>
                    <td>
                        {{ isset($data->ownershipType) ? $data->ownershipType : '' }}
                    </td>
                    <td>{{ isset($data->address) ? $data->address : '' }}</td>
                    <td>{{ isset($data->phone) ? $data->phone : '' }}</td>
                    <td>{{ isset($data->investment) ? $data->investment : '' }}</td>
                    <td>{{ isset($data->quota) ? $data->quota : '' }}</td>
                </tr>
            @endforeach


            </tbody>
        </table>
    </div>
</div>
<div class="row ">
    <h5 class="col-md-12 mb-2">
        Эрхлэх үйл ажиллагааны мэдээлэл
    </h5>
</div>
<div class="row service-legal">
    <div class="col-md-12">
        <table class="table table-bordered ">
            <tbody>
            <tr>
                <th rowspan="2">Одоогийн эсэх</th>
                <th colspan="3">Эрхлэх үйл ажиллагаа</th>
                <th colspan="3">Тусгай зөвшөөрөл</th>
                <th rowspan="2">Бүртгэсэн огноо</th>
            </tr>
            <tr>
                <th>Код</th>
                <th>Нэр</th>
                <th>Үндсэн эсэх</th>
                <th>Олгосон байгууллага</th>
                <th>Олгосон огноо</th>
                <th>Дугаар</th>
            </tr>
            @php
                $list = [];
                if(gettype($response->induty) == 'object')
                $list[] = $response->induty;
                else
                $list = $response->induty;
            @endphp
            @foreach($list as $data )
                <tr>
                    <td>{{ isset($data->isCurrent) ? $data->isCurrent : '' }}</td>
                    <td>{{ isset($data->code) ? $data->code : '' }}</td>
                    <td>{{ isset($data->name) ? $data->name : '' }}</td>
                    <td>{{ isset($data->isMain) ? $data->isMain : '' }}</td>
                    <td>
                        {{ isset($data->permittedInstt) ? $data->permittedInstt : '' }}
                    </td>
                    <td>{{ isset($data->permittedDate) ? $data->permittedDate : '' }}</td>
                    <td>{{ isset($data->permittedNumber) ? $data->permittedNumber : '' }}</td>
                    <td>{{ isset($data->regDate) ? $data->regDate : '' }}</td>
                </tr>
            @endforeach


            </tbody>
        </table>
    </div>
</div>
<div class="row ">
    <h5 class="col-md-12 mb-2">
        Өөрийн хөрөнгийн тухай мэдээлэл
    </h5>
</div>
<div class="row service-legal">
    <div class="col-md-12">
        <table class="table table-bordered ">
            <tbody>
            <tr>
                <th rowspan="2">Одоогийн эсэх</th>
                <th rowspan="2">Өөрийн хөрөнгийн хэмжээ</th>
                <th colspan="2">Үүнээс мянган төгрөгөөр</th>
                <th rowspan="2">Бүртгэсэн огноо</th>
            </tr>
            <tr>
                <th>Төгрөгөөр</th>
                <th>Биет хөрөнгөөр</th>

            </tr>
            @php
                $list = [];
                if(gettype($response->induty) == 'object')
                $list[] = $response->induty;
                else
                $list = $response->induty;
            @endphp
            @foreach($list as $data )
                <tr>
                    <td>{{ isset($data->isCurrent) ? $data->isCurrent : '' }}</td>
                    <td>{{ isset($data->asset) ? $data->asset : '' }}</td>
                    <td>{{ isset($data->cash) ? $data->cash : '' }}</td>
                    <td>{{ isset($data->tangible_asset) ? $data->tangible_asset : '' }}</td>
                    <td>{{ isset($data->regDate) ? $data->regDate : '' }}</td>
                </tr>
            @endforeach


            </tbody>
        </table>
    </div>
</div>
    @if(isset($response->bank))
<div class="row ">
    <h5 class="col-md-12 mb-2">
        Харилцах дансны тухай мэдээлэл
    </h5>
</div>
<div class="row service-legal">
    <div class="col-md-12">
        <table class="table table-bordered ">
            <tbody>
            <tr>
                <th rowspan="2">Одоогийн эсэх</th>
                <th colspan="4">Харилцах данс</th>
                <th rowspan="2">Бүртгэсэн огноо</th>
            </tr>
            <tr>
                <th>Банкны нэр</th>
                <th>Салбарын нэр</th>
                <th>Дансны дугаар</th>
                <th>Дансны төрөл</th>
            </tr>
            @php
                $list = [];
                if(gettype($response->bank) == 'object')
                $list[] = $response->bank;
                else
                $list = $response->bank;
            @endphp
            @foreach($list as $data )
                <tr>
                    <td>{{ isset($data->isCurrent) ? $data->isCurrent : '' }}</td>
                    <td>{{ isset($data->bankTitle) ? $data->bankTitle : '' }}</td>
                    <td>{{ isset($data->bankBranch) ? $data->bankBranch : '' }}</td>
                    <td>{{ isset($data->bankAccountNo) ? $data->bankAccountNo : '' }}</td>
                    <td>{{ isset($data->bankAccountType) ? $data->bankAccountType : '' }}</td>
                    <td>{{ isset($data->regDate) ? $data->regDate : '' }}</td>
                </tr>
            @endforeach


            </tbody>
        </table>
    </div>
</div>
    @endif

    @if(isset($response->generalR))
    <div class="row service-legal">
        <h5 class="col-md-12 mb-2">
            Харилцах дансны тухай мэдээлэл
        </h5>
    </div>
    <div class="row ">
        <div class="col-md-12">
            <table class="table table-bordered ">
                <tbody>
                <tr>
                    <th rowspan="2">Одоогийн эсэх</th>
                    <th colspan="5">Итгэмжлэлгүйгээр төлөөлөх этгээд</th>
                    <th rowspan="2">Бүртгэсэн огноо</th>
                </tr>
                <tr>
                    <th>Эцэг/эхийн нэр</th>
                    <th>Өөрийн нэр</th>
                    <th>Регистр</th>
                    <th>Улсын нэр</th>
                    <th>Албан тушаал</th>
                </tr>
                @php
                    $list = [];
                    if(gettype($response->generalR) == 'object')
                    $list[] = $response->generalR;
                    else
                    $list = $response->generalR;
                @endphp
                @foreach($list as $data )
                    <tr>
                        <td>{{ isset($data->isCurrent) ? $data->isCurrent : '' }}</td>
                        <td>{{ isset($data->lastname) ? $data->lastname : '' }}</td>
                        <td>{{ isset($data->firtname) ? $data->firtname : '' }}</td>
                        <td>{{ isset($data->registerNo) ? $data->registerNo : '' }}</td>
                        <td>{{ isset($data->country) ? $data->country : '' }}</td>
                        <td>{{ isset($data->position) ? $data->position : '' }}</td>
                        <td>{{ isset($data->regDate) ? $data->regDate : '' }}</td>
                    </tr>
                @endforeach


                </tbody>
            </table>
        </div>
    </div>
    @endif
    @if(isset($response->branch))
    <div class="row ">
        <h5 class="col-md-12 mb-2">
            Салбарын тухай мэдээлэл
        </h5>
    </div>
    <div class="row service-legal">
        <div class="col-md-12">
            <table class="table table-bordered ">
                <tbody>
                <tr>
                    <th rowspan="2">ҮА явуулж байгаа эсэх</th>
                    <th colspan="3">Салбар</th>
                    <th colspan="3">Удирдлагын</th>
                    <th rowspan="2">Бүртгэсэн огноо</th>
                </tr>
                <tr>
                    <th>Нэр</th>
                    <th>Хаяг</th>
                    <th>Утасны дугаа</th>
                    <th>Эцэг/эхийн нэр</th>
                    <th>Өөрийн нэр</th>
                    <th>Регитр</th>
                </tr>
                @php
                    $list = [];
                    if(gettype($response->branch) == 'object')
                    $list[] = $response->branch;
                    else
                    $list = $response->branch;
                @endphp
                @foreach($list as $data )
                    <tr>
                        <td>{{ isset($data->doesSend) ? $data->doesSend : '' }}</td>
                        <td>{{ isset($data->title) ? $data->title : '' }}</td>
                        <td>{{ isset($data->address) ? $data->address : '' }}</td>
                        <td>{{ isset($data->phone) ? $data->phone : '' }}</td>
                        <td>{{ isset($data->lastname) ? $data->lastname : '' }}</td>
                        <td>{{ isset($data->firstname) ? $data->firstname : '' }}</td>
                        <td>{{ isset($data->registerNo) ? $data->registerNo : '' }}</td>
                        <td>{{ isset($data->regDate) ? $data->regDate : '' }}</td>
                    </tr>
                @endforeach


                </tbody>
            </table>
        </div>
    </div>
    @endif
    @if(isset($response->changeFond))
    <div class="row ">
        <h5 class="col-md-12 mb-2">
            Өөрчлөн зохион байгуулагдсан тухай мэдээлэл
        </h5>
    </div>
    <div class="row service-legal">
        <div class="col-md-12">
            <table class="table table-bordered ">
                <tbody>
                <tr>
                    <th rowspan="2">Өөрчлөн ЗБ хэлбэр</th>
                    <th colspan="2">Өөрчлөлтын өмнөх</th>
                    <th colspan="3">Өөрчлөлтийн дараа</th>

                </tr>
                <tr>
                    <th>Регистер</th>
                    <th>Нэр</th>
                    <th>Регистер</th>
                    <th>Нэр</th>
                    <th>Эрх залгамжлах хувь</th>
                </tr>
                @php
                    $list = [];
                    if(gettype($response->changeFond) == 'object')
                    $list[] = $response->changeFond;
                    else
                    $list = $response->changeFond;
                @endphp
                @foreach($list as $data )
                    <tr>
                        <td>{{ isset($data->status) ? $data->status : '' }}</td>
                        <td>{{ isset($data->old_cpr_no) ? $data->old_cpr_no : '' }}</td>
                        <td>{{ isset($data->old_cpr_name) ? $data->old_cpr_name : '' }}</td>
                        <td>{{ isset($data->new_cpr_no) ? $data->new_cpr_no : '' }}</td>
                        <td>{{ isset($data->new_cpr_name) ? $data->new_cpr_name : '' }}</td>
                        <td>{{ isset($data->mndt_qota) ? $data->mndt_qota : '' }}</td>
                    </tr>
                @endforeach


                </tbody>
            </table>
        </div>
    </div>
    @endif
    @if(isset($response->liquidation))
    <div class="row ">
        <h5 class="col-md-12 mb-2">
            Өөрчлөн зохион байгуулагдсан тухай мэдээлэл
        </h5>
    </div>
    <div class="row service-legal">
        <div class="col-md-12">
            <table class="table table-bordered ">
                <tbody>
                <tr>
                    <th rowspan="2">Байгууллагын нэр</th>
                    <th colspan="2">Татан буугдсан тухай шийдвэр</th>
                    <th rowspan="2">Хэвлэл мэдээллийн хэрэгслээр зарлуулсан огноо</th>
                    <th colspan="2">Бүртгэсэн</th>
                    <th rowspan="2">Улсын бүртгэлээс хассан огноо</th>
                </tr>
                <tr>
                    <th>Огноо</th>
                    <th>Дугаар</th>
                    <th>Огноо</th>
                    <th>Дугаар</th>
                </tr>


                    <tr>
                        <td>{{ isset($response->liquidation->companyTitle) ? $response->liquidation->companyTitle : '' }}</td>
                        <td>{{ isset($response->liquidation->liqDecitionDate) ? $response->liquidation->liqDecitionDate : '' }}</td>
                        <td>{{ isset($response->liquidation->liqDecitionNo) ? $response->liquidation->liqDecitionNo : '' }}</td>
                        <td>{{ isset($response->liquidation->publicAnnouncementDate) ? $response->liquidation->publicAnnouncementDate : '' }}</td>
                        <td>{{ isset($response->liquidation->regDate) ? $response->liquidation->regDate : '' }}</td>
                        <td>{{ isset($response->liquidation->regNo) ? $response->liquidation->regNo : '' }}</td>
                        <td>{{ isset($response->liquidation->removeDate) ? $response->liquidation->removeDate : '' }}</td>
                    </tr>



                </tbody>
            </table>
        </div>
    </div>
    @endif
    @if(isset($response->changeName))
        <div class="row ">
        <h5 class="col-md-12 mb-2">
            Нэр өөрчилсөн тухай мэдээлэл
        </h5>
        </div>
        <div class="row service-legal">
        <div class="col-md-12">
            <table class="table table-bordered ">
                <tbody>
                <tr>
                    <th>Хуулийн этгээдийн нэр</th>
                    <th>Хуулийн этгээдийн хэлбэр</th>
                    <th>Өөрчлөх шалтгаан</th>
                    <th>Бүртгэсэн огноо</th>
                </tr>

                @php
                    $list = [];
                    if(gettype($response->changeName) == 'object')
                    $list[] = $response->changeName;
                    else
                    $list = $response->changeName;
                @endphp
                @foreach($list as $data )
                    <tr>
                        <td>{{ isset($data->name) ? $data->name : '' }}</td>
                        <td>{{ isset($data->form) ? $data->form : '' }}</td>
                        <td>{{ isset($data->reason) ? $data->reason : '' }}</td>
                        <td>{{ isset($data->regDate) ? $data->regDate : '' }}</td>
                    </tr>
                @endforeach


                </tbody>
            </table>
        </div>
        </div>
    @endif