<div class="row">
    <h4 class="col-md-12 mb-3">
        Тээврийн хэрэгслийн татварын мэдээлэл
    </h4>
</div>
<div class="row">
    <div class="col-md-12">
        <table class="table table-striped">
            <tbody>
            @php
                $list = [];
                if(gettype($response->listData) == 'object')
                    $list[] = $response->listData;
                else
                    $list = $response->listData;

            @endphp

            @foreach($list as $data)
                <tr>
                    <th class="text-left">Тээврийн хэрэгсэл:</th>
                    <th class="text-right font-weight-bold">{{ isset($data->mark) ? $data->mark : '' }} {{ isset($data->model) ? $data->model : '' }} - {{ isset($data->autoNumber) ? $data->autoNumber : '' }}</th>
                </tr>
                <tr>
                    <td class="text-left">Арлын дугаар:</td>
                    <td class="text-right font-weight-bold">{{ isset($data->aralNo) ? $data->aralNo : '' }}</td>
                </tr>
                <tr>
                    <td class="text-left">Эзэмшигчийн овог нэр:</td>
                    <td class="text-right font-weight-bold">{{ isset($data->lastName) ? $data->lastName : '' }} {{ isset($data->firstName) ? $data->firstName : '' }}</td>
                </tr>
                <tr>
                    <td class="text-left">Өнгө:</td>
                    <td class="text-right font-weight-bold">{{ isset($data->colorName) ? $data->colorName : '' }}</td>
                </tr>
                <tr>
                    <td class="text-left">Утас:</td>
                    <td class="text-right font-weight-bold">{{ isset($data->phone) ? $data->phone : '' }}</td>
                </tr>
                <tr>
                    <td class="text-left">Регистрийн дугаар:</td>
                    <td class="text-right font-weight-bold">{{ isset($data->regnum) ? $data->regnum : '' }}</td>
                </tr>
                <tr>
                    <td class="text-left">vehicleId:</td>
                    <td class="text-right font-weight-bold">{{ isset($data->vehicleId) ? $data->vehicleId : '' }}</td>
                </tr>
                <tr>
                    <td class="text-left">Төрөл:</td>
                    <td class="text-right font-weight-bold">{{ isset($data->carType) ? $data->carType : ''  }}</td>
                </tr>

            @endforeach
            </tbody>
        </table>
    </div>
</div>
