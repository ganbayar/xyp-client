<div class="row">
    <h4 class="col-md-12 mb-3">
        Тээврийн хэрэгслийн торгуулийн мэдээлэл
    </h4>
</div>
<div class="row">
    <div class="col-md-12">
        <table class="table table-striped">
            <tbody>
            <tr>
                <th>Огноо</th>
                <th>Дүн</th>
                <th>Шалтгаан</th>
                <th>Байршил</th>
                <th></th>
            </tr>

            @php
                $list = $response->list;
                usort($list, function ($item1, $item2) {
                    return $item2->passDate <=> $item1->passDate;
                });
            @endphp

            @foreach($list as $data)

                <tr>
                    <td>{{ date('Y-m-d', strtotime($data->passDate)) }}</td>
                    <td>{{ $data->amount }}</td>
                    <td>{{ $data->reasonType }}</td>
                    <td>{{ $data->localName }}</td>
                    <td>{{ $data->paymentStatus != 0 ? 'Төлсөн' : 'Төлөөгүй' }}</td>

                </tr>
            @endforeach

            </tbody>
        </table>
    </div>
</div>
