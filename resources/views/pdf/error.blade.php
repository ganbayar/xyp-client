<div class="row">
    <h4 class="col-md-12 mb-3">
        {{ $title }}
    </h4>
</div>
<div class="row">
    <div class="col-md-12 col-xs-12">
        <p style="font-size: 15px;"><strong>{{$regnum}}</strong> регистрийн дугаартай үйлчлүүлэгчийн {{ $title }} <strong>{{mb_strtolower($message) }}</strong>.</p>
    </div>
</div>