@extends('layouts.pdf')
@section('content')



    <div class="card">
        <div class="card-body">
            <div class="vtabs customvtab">
                <!-- Tab panes -->
                <div class="tab-content printableArea">
                    @foreach($service_output as $ws_code => $value)
                        <div class="tab-pane {{ $loop->first ? 'active' : '' }}" id="{{$ws_code}}" role="tabpanel">
                            <div class="">
                                @include('pdf.header')
                                @if($value['result']->resultCode == 0)
                                    @include('pdf.'. $ws_code, ['response' => $value['result']->response, 'request' => $value['result']->request])
                                @else
                                    @php $message = '' @endphp
                                    @switch($value['result']->resultCode)
                                        @case(1) @php $message = 'Үр дүн олдсонгүй' @endphp @break
                                        @case(2) @php $message = 'Шалгалтын явцад алдаа гарлаа' @endphp @break
                                        @case(3) @php $message = 'Баталгаажуулалт буруу' @endphp @break
                                        @case(301) @php $message = 'Хурууны хээ олдсонгүй' @endphp @break
                                        @case(302) @php $message = 'Мэдээлэл зөрүүтэй байна' @endphp @break
                                        @case(303) @php $message = 'Хурууны хээ тулгах процесс хэт удаан' @endphp @break
                                        @case(304) @php $message = 'Хурууны хээ тулгахад алдаа гарлаа' @endphp @break
                                        @case(402) @php $message = 'Ззэмшигчийн мэдээлэл зөрүүтэй' @endphp @break
                                        @default @php $message = 'Алдаа' @endphp
                                    @endswitch
                                    @include('pdf.error', ['title'=>$value['name'],'code' => $value['result']->resultCode, 'message' => $message,'regnum'=>$value['regnum']])
                                @endif
                                @php
                                    $requestId = isset($value['result']->requestId) ? $value['result']->requestId : 0;
                                @endphp
                                @include('pdf.footer', ['id' => $requestId ])
                            </div>
                        </div>
                    @endforeach


                </div>

            </div>
        </div>

    </div>
@endsection
@push('styles')
<link href="{{ public_path('/dist/css/pages/tab-page.css') }}" rel="stylesheet">
@endpush
