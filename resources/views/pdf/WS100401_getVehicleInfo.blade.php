<div class="row">
    <h4 class="col-md-12 mb-3">
        Тээврийн хэрэгслийн мэдээлэл
    </h4>
</div>
<div class="row">
    <div class="col-md-12">
        <table class="table table-striped">
            <tbody>
            
                <tr>
                    <th class="text-left">Тээврийн хэрэгсэл:</th>
                    <th class="text-right font-weight-bold">{{ $response->markName }} {{ $response->modelName }} - {{ $response->plateNumber }}</th>
                </tr>
                <tr>
                    <td class="text-left">Үйлдвэрлэсэн он:</td>
                    <td class="text-right font-weight-bold">{{ $response->buildYear }}</td>
                </tr>
                <tr>
                    <td class="text-left">Арлын дугаар:</td>
                    <td class="text-right font-weight-bold">{{ $response->cabinNumber }}</td>
                </tr>
                <tr>
                    <td class="text-left">Хөдөлгүүрийн багтаамж:</td>
                    <td class="text-right font-weight-bold">{{ $response->capacity }}</td>
                </tr>
                <tr>
                    <td class="text-left">Ангилал:</td>
                    <td class="text-right font-weight-bold">{{ $response->className }}</td>
                </tr>
                <tr>
                    <td class="text-left">Өнгө:</td>
                    <td class="text-right font-weight-bold">{{ $response->colorName }}</td>
                </tr>
                <tr>
                    <td class="text-left">Улс:</td>
                    <td class="text-right font-weight-bold">{{ $response->countryName }}</td>
                </tr>
                <tr>
                    <td class="text-left">Мотрын төрөл:</td>
                    <td class="text-right font-weight-bold">{{ $response->fueltype }}</td>
                </tr>
                <tr>
                    <td class="text-left">Оруулж ирсэн огноо:</td>
                    <td class="text-right font-weight-bold">{{ date('Y-m-d', strtotime($response->importDate))  }}</td>
                </tr>
                <tr>
                    <td class="text-left">Суудлын тоо:</td>
                    <td class="text-right font-weight-bold">{{ $response->manCount  }}</td>
                </tr>
                <tr>
                    <td class="text-left">Дамжуулалт:</td>
                    <td class="text-right font-weight-bold">{{ $response->transmission  }}</td>
                </tr>
                <tr>
                    <td class="text-left">Төрөл:</td>
                    <td class="text-right font-weight-bold">{{ $response->type  }}</td>
                </tr>
                <tr>
                    <td class="text-left">Хүрдний байрлал:</td>
                    <td class="text-right font-weight-bold">{{ $response->wheelPosition  }}</td>
                </tr>
                <tr>
                    <td class="text-left">Жин:</td>
                    <td class="text-right font-weight-bold">{{ $response->mass  }}</td>
                </tr>
            
            </tbody>
        </table>
    </div>
</div>
