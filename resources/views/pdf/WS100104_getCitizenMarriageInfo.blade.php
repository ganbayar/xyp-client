<div class="row">
    <h4 class="col-md-12 mb-3">
        Гэрлэлтийн мэдээлэл
    </h4>
</div>
<div class="row">
    <div class="col-md-12">
        <table class="table table-striped">
            <tbody>
            <tr>
                <td class="text-left">Гэрлэлтийн төлөв:</td>
                <td class="text-right font-weight-bold" colspan="2">{{ $response->married? 'Гэрэлсэн' : 'Гэрлээгүй' }}</td>
            </tr>
            <tr>
                <td class="text-left">Гэрэлсэн огноо:</td>
                <td class="text-right font-weight-bold" colspan="2">{{ isset($response->marriedDate) ? date('Y-m-d', strtotime($response->marriedDate)) : '' }}</td>
            </tr>
            <tr>
                <td class="text-left">Бүртгэсэн газар:</td>
                <td class="text-right font-weight-bold" colspan="2">{{ isset($response->registeredCity) ? $response->registeredCity : '' }} {{ isset($response->registeredDistrict) ? $response->registeredDistrict : '' }}</td>
            </tr>
            <tr>
                <td class="text-left">Бүртгэсэн огноо:</td>
                <td class="text-right font-weight-bold" colspan="2">{{ isset($response->registeredDate) ?  date('Y-m-d', strtotime($response->registeredDate)) : '' }}</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <h5 class="col-md-12 text-left mb-3">
        Нөхөр
    </h5>
</div>
<div class="row">
    <div class="col-md-12">
        <table class="table table-striped">
            <tbody>

            <tr>
                <td class="text-left">Овог:</td>
                <td class="text-right font-weight-bold">{{ $response->husband->lastname }}</td>
            </tr>
            <tr>
                <td class="text-left">Нэр:</td>
                <td class="text-right font-weight-bold">{{ $response->husband->firstname }}</td>
            </tr>
            <tr>
                <td class="text-left">Регистер:</td>
                <td class="text-right font-weight-bold">{{ $response->husband->regnum }}</td>
            </tr>
            <tr>
                <td class="text-left">Өмнөх төлөв:</td>
                <td class="text-right font-weight-bold">{{ $response->husband->marriageStatus }}</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <h5 class="col-md-12 text-left mb-3">
        Эхнэр
    </h5>
</div>
<div class="row">
    <div class="col-md-12">
        <table class="table table-striped">
            <tbody>

            <tr>
                <td class="text-left">Овог:</td>
                <td class="text-right font-weight-bold">{{ $response->wife->lastname }}</td>
            </tr>
            <tr>
                <td class="text-left">Нэр:</td>
                <td class="text-right font-weight-bold">{{ $response->wife->firstname }}</td>
            </tr>
            <tr>
                <td class="text-left">Регистер:</td>
                <td class="text-right font-weight-bold">{{ $response->wife->regnum }}</td>
            </tr>
            <tr>
                <td class="text-left">Өмнөх төлөв:</td>
                <td class="text-right font-weight-bold">{{ $response->wife->marriageStatus }}</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
