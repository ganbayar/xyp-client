<div class="row">
    <h4 class="col-md-12 mb-3">
        Төрсний бүртгэлийн мэдээлэл
    </h4>
</div>
<div class="row">
    <div class="col-md-12">
        <table class="table table-striped">
            <tbody>
            <tr>
                <td>Төрсөн он сар өдөр</td>
                <td colspan="2">{{ date('Y-m-d', strtotime($response->birthDateAsText)) }}</td>
            </tr>
            <tr>
                <td>Төрсөн газар</td>
                <td colspan="2">{{ $response->birthPlace }}</td>
            </tr>
            <tr>
                <td>Хүйс:</td>
                <td colspan="2">{{ $response->gender == 1 ? 'эрэгтэй' : 'эмэгтэй' }}</td>
            </tr>
            <tr>
                <td>Овог:</td>
                <td colspan="2">{{ $response->lastname }}</td>
            </tr>
            <tr>
                <td>Нэр:</td>
                <td colspan="2">{{ $response->firstname }}</td>
            </tr>
            <tr>
                <td>Регистер:</td>
                <td colspan="2">{{ mb_strtoupper($response->regnum, 'UTF-8')  }}</td>
            </tr>
            <tr>
                <td>Бүртгэсэн огноо:</td>
                <td colspan="2">{{ date('Y-m-d', strtotime($response->registrationDate))  }}</td>
            </tr>
            <tr>
                <td>Бүртгэсэн газар:</td>
                <td colspan="2">{{ $response->registrationCity }}/{{ $response->registrationDistrict }}</td>
            </tr>
            <tr>
                <td>Бүртгэлийн дугаар:</td>
                <td colspan="2">{{ $response->registrationId }}</td>
            </tr>
            <tr>
                <td rowspan="4" style="vertical-align: middle">Эцгийн</td>
                <td>овог:</td>
                <td></td>
            </tr>
            <tr>

                <td>Эцэг/эх/-ийн нэр:</td>
                <td>
                    @if(isset($response->father->lastname))
                        {{ $response->father->lastname }}
                    @endif
                </td>
            </tr>
            <tr>

                <td>Нэр:</td>
                <td>
                    @if(isset($response->father->firstname))
                        {{ $response->father->firstname }}
                    @endif
                </td>
            </tr>
            <tr>

                <td>Регистер:</td>
                <td>
                    @if(isset($response->father->regnum))
                        {{ mb_strtoupper($response->father->regnum, 'UTF-8')  }}
                    @endif
                </td>
            </tr>
            <tr>
                <td rowspan="4" style="vertical-align: middle">Эхийн</td>
                <td>овог:</td>
                <td>

                </td>
            </tr>
            <tr>

                <td>Эцэг/эх/-ийн нэр:</td>
                <td>
                    @if(isset($response->mother->lastname))
                        {{ $response->mother->lastname  }}
                    @endif
                </td>
            </tr>
            <tr>

                <td>Нэр:</td>
                <td>
                    @if(isset($response->mother->firstname))
                        {{ $response->mother->firstname  }}
                    @endif
                </td>
            </tr>
            <tr>

                <td>Регистер:</td>
                <td>
                    @if(isset($response->mother->regnum))
                        {{ mb_strtoupper($response->mother->regnum, 'UTF-8')  }}
                    @endif
                </td>
            </tr>



            </tbody>
        </table>

    </div>
</div>
