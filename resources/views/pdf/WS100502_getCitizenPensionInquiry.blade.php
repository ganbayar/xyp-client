<div class="row">
    <h4 class="col-md-12 mb-3">
        Тэтгэврийн мэдээлэл
    </h4>
</div>
<div class="row">
    <div class="col-md-12">
        @if( $response->pensioner)
            <p><i>{{ $request->regnum }}</i> регистрийн дугаартай иргэн нь тэтгэвэрт <b>гарсан</b> болно</p>
        @else
            <p>{{ $request->regnum }} регистрийн дугаартай иргэн нь тэтгэвэрт <b>гараагүй</b> болно</p>
        @endif
    </div>
</div>