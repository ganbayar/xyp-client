<div class="row service-footer">
    <div class="col-md-12">
        <div class="visible-print">
            {!! QrCode::size(150)->generate(config('app.url').'/api/checkQr/'.$id) !!}
            <p style="padding-bottom: 50px; display: inline;">Тухайн 'qr code' -ийн тусламжтай шалгах боломжтой.</p>
        </div>
    </div>
</div>