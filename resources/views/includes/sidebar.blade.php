<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="user-pro"> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><img src="/images/user.png" alt="user-img" class="img-circle"><span class="hide-menu">{{ Auth::user()->name }}</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();"><i class="fa fa-power-off"></i>
                                Гарах
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form></li>
                    </ul>
                </li>
                <li class="nav-small-cap">--- Үндсэн цэс </li>

                <li> <a href="{{ url('service') }}" id="toggler" class="waves-effect"><i class=" icon-doc zmdi-hc-fw fa-fw"></i> <span class="hide-menu">Мэдээлэл авах</span></a> </li>
                @if(Config::get('app.env') == 'demo')
                <li> <a href="{{ url('/service/client/lookup') }}" id="toggler" class="waves-effect"><i class=" icon-doc zmdi-hc-fw fa-fw"></i> <span class="hide-menu">Өндөр эрсдэлтэй харилцагч хайх</span></a> </li>
                @endif

                @if(Auth::user()->is_superuser == 1)
                    <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="icon-user zmdi-hc-fw fa-fw"></i><span class="hide-menu">Хэрэглэгч</span></a>
                        <ul aria-expanded="false" class="collapse">
                            <li> <a href="{{ url('/user/list') }}">Хэрэглэгчдийн жагсаалт</a> </li>
                            <li> <a href="{{ url('/ugroup/list') }}">Бүлэг</a> </li>
                        </ul>
                    </li>
                    <li> <a href="{{ url('group') }}" class="waves-effect"><i class=" icon-book-open zmdi-hc-fw fa-fw"></i> <span class="hide-menu">Үйлчилгээ үүсгэх</span></a> </li>
                    <li> <a href="{{ url('client') }}" class="waves-effect"><i class=" icon-link zmdi-hc-fw fa-fw"></i> <span class="hide-menu">API Клиент</span></a> </li>
                    <li> <a href="{{ url('service_log') }}" class="waves-effect"><i class=" icon-chart zmdi-hc-fw fa-fw"></i> <span class="hide-menu">Үйлчилгээний лог</span></a> </li>
                    <li> <a href="{{ url('log') }}" class="waves-effect"><i class=" icon-chart zmdi-hc-fw fa-fw"></i> <span class="hide-menu">Лог бүртгэл</span></a> </li>
                @endif
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
