@extends('layouts.master')
@section('content')

    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Хэрэглэгч      </h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">

            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="table-responsive m-t-40">
                <table id="myTable" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Нэр</th>
                            <th>И-мэйл хаяг</th>
                            <th>Системд нэвтрэх нэр</th>
                            <th>Регистрийн дугаар</th>
                            <th>Хандах эрх</th>
                            <th>Бүлэг</th>
                            <th>Сүүлд хандсан огноо</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->username }}</td>
                            <td>{{ $user->regnum }}</td>
                            @if($user->is_superuser == 1)
                                <td><span class="label label-rounded label-info">админ</span></td>
                            @else
                                <td><span class="label label-rounded label-info">үйлчилгээний ажилтан</span></td>
                            @endif
                            <td>
                                <button  type="button"
                                         class="btn btn-info btn-circle"
                                         data-target="#modalframe"
                                         data-toggle="modal"
                                         data-src="{{ url('/user/ugroup/'.$user->id) }}"
                                ><i class="icon-settings"></i></button>
                            </td>
                            </td>
                            <td>{{ $user->last_login }}</td>
                            <td>
                                <button  type="button"
                                         class="btn btn-info btn-circle"
                                         data-title="засах"
                                         data-target="#modalEdit"
                                         data-toggle="modal"
                                         data-id="{{ $user->id }}"
                                         data-regnum = "{{ $user->regnum }}"
                                         data-perm = "{{ $user->is_superuser }}"
                                ><i class="icon-note"></i></button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <h4 class="modal-title text-white" id="myLargeModalLabel">Хэрэглэгч засах</h4>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <form class="form-material" action="" method="post" id="form">
                        <input type="hidden" name="_method" value="PUT">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label>Регистрийн дугаар</label>
                            <input type="text" class="form-control" name="regnum" id="regnum">
                        </div>
                        <div class="form-group">
                            <label>Хандах эрх</label>
                            <select class="form-control" name="perm" id="perm">
                                <option value="0">хэрэглэгч</option>
                                <option value="1">админ</option>
                            </select>
                        </div>
                        <div class="form-group row">
                            <div class="offset-sm-6 col-sm-3">
                                <button class="btn btn-info waves-effect text-left" type="submit">засах</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="modalframe" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <h4 class="modal-title text-white" id="myLargeModalLabel">Бүлэгт хэрэглэгч нэмэх</h4>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <iframe width="100%" height="400" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('styles')
    <link href="../assets/node_modules/datatables/media/css/dataTables.bootstrap4.css" rel="stylesheet">
@endpush
@push('scripts')
    <script src="/dist/js/jquery.maskedinput.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $(function(){
                $("#regnum").mask("cc99999999");
            });
        });
    </script>
    <script src="../assets/node_modules/datatables/datatables.min.js"></script>

    <script>
        $(function() {
            $('#myTable').DataTable({
                "language" : {
                    "emptyTable":     "Мэдээлэл олдсонгүй",
                    "info":           "Дэлгэцэнд _START_ to _END_ of _TOTAL_ хооронд харуулж байна",
                    "infoEmpty":      "Дэлгэцэнд 0 to 0 of 0 хооронд харуулж байна",
                    "infoFiltered":   "(Нийт _MAX_ мэдээллээс шүүлт хийв)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Дэлгэцлэх _MENU_ мөрийн тоо",
                    "loadingRecords": "Ачааллаж байна...",
                    "processing":     "Тооцоолж байна...",
                    "search":         "Хайх:",
                    "zeroRecords":    "Хайлтын үр дүн олдсонгүй",
                    "paginate": {
                        "first":      "Эхэнд",
                        "last":       "Сүүлд",
                        "next":       "Дараагийн хуудас",
                        "previous":   "Өмнөх хуудас"
                    },
                    "aria": {
                        "sortAscending":  ": Өсөх дарааллаар эрэмблэх",
                        "sortDescending": ": Буурах дарааллаар эрэмблэх"
                    },
                    "fnDrawCallback": function( oSettings ) {
                        alert( 'DataTables has redrawn the table' );
                    }
                },
                "pageLength": 20,
                "lengthMenu": [[20, 40, 60, -1], [20, 40, 60, "All"]]
            });
        });
    </script>
    <script>
        $('#modalframe').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var src = button.data('src')
            var modal = $(this)
            modal.find('.modal-body iframe').attr('src', src);
        });

        $('#modalEdit').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var id = button.data('id')
            var regnum = button.data('regnum')
            var perm = button.data('perm')

            var modal = $(this)
            modal.find('.modal-content #form').attr('action', '/user/list/' + id);
            modal.find('.modal-body input#regnum').val(regnum);
            modal.find('.modal-body select#perm').val(perm);
        });
    </script>
@endpush