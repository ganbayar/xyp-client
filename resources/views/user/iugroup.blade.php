@extends('layouts.iframe')
@section('content')
    <form id="form" target="_top" class="form-horizontal" action="{{ url('user/ugroup/update') }}" method="post">
        {{ csrf_field() }}
        <input type="hidden" name="user_id" value="{{ $user }}">
        <div class="form-group">
            <select id='pre-selected-options' multiple='multiple' class="" name="ugroups[]">
                @foreach($ugroups as $ugroup)
                    @if(in_array($ugroup->id, $selected))
                        <option value="{{ $ugroup->id }}" selected>{{ $ugroup->group_name }}</option>
                    @else
                        <option value="{{ $ugroup->id }}" >{{ $ugroup->group_name }}</option>
                    @endif
                @endforeach
            </select>

        </div>
        <div class="form-group row">
            <div class="offset-sm-3 col-sm-9">
                <button class="btn btn-info waves-effect waves-light m-t-10"  type="submit">хадгалах</button>
            </div>
        </div>
    </form>
@endsection