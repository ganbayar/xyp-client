@extends('layouts.master')
@section('content')
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Үйлчилгээ удирдлага</h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">

                <button data-target="#modalAdd"  data-toggle="modal" type="button" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Үйлчилгээ бүлэглэх</button>
            </div>
        </div>
    </div>


    <div class="row">
        @foreach($groups as $group)
            <div class="col-lg-3 col-md-3">
                <div class="card text-white bg-info">
                    <div class="card-body">
                        <h4 class="text-white">{{ $group->title }} <a class="get-code" data-toggle="collapse" href="#tt-{{$group->id}}" aria-expanded="true"><i class="fas fa-info-circle" title="Дэлгэрэнгүй" data-toggle="tooltip"></i></a></h4>
                        <div class="collapse m-t-15" id="tt-{{$group->id}}" aria-expanded="true">
                            <ul class="list-group">
                                @foreach($group->services as $service)
                                    <li class="list-group-item" style="color:#2f7396;"> {{ $service->title }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="card-footer group-footer">
                        <div class="row" style="text-align: center;">
                            <div class="col-md-6">
                                <button data-target="#modalEdit"  data-toggle="modal" type="button" class="btn btn-success  m-l-15" title="засах"
                                        data-id="{{ $group->id }}"
                                        data-group_name="{{ $group->title }}"
                                        data-webs="{{ $group->services }}"><i class="icon-note"></i>
                                </button>
                            </div>
                            <div class="col-md-6">
                                <form action="{{ url('/group/'.$group->id) }}" method="post">
                                    <input type="hidden" name="_method" value="DELETE">
                                    {{ csrf_field() }}
                                    <button class="btn btn-danger  m-l-15" onclick="return confirm('Устгах уу?')" type="submit" title="устгах" ><i class="icon-trash" ></i></button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>



    <div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <h4 class="modal-title text-white" id="myLargeModalLabel">Үйлчилгээ бүлэглэх</h4>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="skin skin-square">
                        <form class="form-material" action="{{ route('group.store') }}" method="post">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label>Нэр</label>
                                <input type="text" class="form-control" name="group_name" required="required">
                            </div>
                            <div class="input-group">
                                <ul class="icheck-list" multiple="multiple">
                                    @foreach($webservices as $webservice)
                                        <li>
                                            <input  name="webs[]" type="checkbox" value="{{$webservice->id}}" class="check" id="minimal-checkbox-{{ $loop->index+1 }}" data-checkbox="icheckbox_square-red">
                                            <label for="minimal-checkbox-{{ $loop->index+1 }}">{{ $webservice->title }}</label>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <hr>
                            <div class="form-group row">
                                <div class="offset-sm-6 col-sm-3">
                                    <button class="btn btn-info waves-effect text-left" type="submit">үүсгэх</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel">Бүлэг засах</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="skin skin-square">
                        <form id="form" class="form-material" action="" method="post">
                            <input type="hidden" name="_method" value="PUT">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label>Нэр</label>
                                <input type="text" class="form-control" name="group_name" id="group_name" required="required">
                            </div>
                            <div class="input-group">
                                <ul class="icheck-list" multiple="multiple">
                                    @foreach($webservices as $webservice)
                                        <li>
                                            <input  name="webs[]" id="webs-{{ $webservice->id }}" type="checkbox" value="{{$webservice->id}}" class="check" id="minimal-checkbox-{{ $loop->index+1 }}" data-checkbox="icheckbox_square-red">
                                            <label for="minimal-checkbox-{{ $loop->index+1 }}">{{ $webservice->title }}</label>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="form-group row">
                                <div class="offset-sm-6 col-sm-3">
                                    <button class="btn btn-info waves-effect text-left" type="submit">засах</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('styles')
    <link href="/assets/node_modules/icheck/skins/all.css" rel="stylesheet">
    <link href="/dist/css/pages/form-icheck.css" rel="stylesheet">
    <link href="/dist/css/pages/stylish-tooltip.css" rel="stylesheet">
    <style>
        .group-footer{
            border-top: 1px solid #fff !important;
        }
    </style>
@endpush
@push('scripts')
    <script src="/assets/node_modules/icheck/icheck.min.js"></script>
    <script src="/assets/node_modules/icheck/icheck.init.js"></script>

    <script>
        $(document).ready(function() {
            $('#modalEdit').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget)
                var id = button.data('id')
                var group_name = button.data('group_name')
                var webs = button.data('webs')

                var modal = $(this)
                modal.find('.modal-content #form').attr('action', '/group/' + id);
                modal.find('.modal-body input#group_name').val(group_name);
                webs.forEach(function(element) {
                    $('#webs-'+element.id+'').prop('checked',true);
                    $('#webs-'+element.id+'').closest('div').addClass('checked');
                });
            });
        });
    </script>
@endpush