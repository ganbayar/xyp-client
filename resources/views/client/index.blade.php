@extends('layouts.master')
@section('content')
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">API Клиент</h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <button data-target="#modalAdd"  data-toggle="modal" type="button" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Клиент нэмэх</button>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="table-responsive m-t-40">
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>№</th>
                        <th>Хандах токен</th>
                        <th>Тайлбар</th>
                        <th>Идэвхтэй эсэх</th>
                        <th>Дуусах огноо</th>
                        <th>Бүртгэгдсэн огноо</th>
                        <th>Засварлагдсан огноо</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($clients as $client)
                        <tr>
                            <td>{{ $loop->index+1 }}</td>
                            <td><a class="token" data-val="{{ $client->access_token }}"></a></td>
                            <td>{{ $client->description }}</td>
                            @if($client->is_active == 1)
                                <td><span class="label label-rounded label-info">тийм</span></td>
                            @else
                                <td><span class="label label-rounded label-danger">үгүй</span></td>
                            @endif
                            </td>
                            <td>{{ $client->expire_date }}</td>
                            <td>{{ $client->created_at }}</td>
                            <td>{{ $client->updated_at }}</td>
                            <td>
                                <button  type="button"
                                         class="btn btn-info btn-circle"
                                         data-title="засах"
                                         data-target="#modalEdit"
                                         data-toggle="modal"
                                         data-id="{{ $client->id }}"
                                         data-expire = "{{ $client->expire_date }}"
                                         data-is_active = "{{ $client->is_active }}"
                                         data-description = "{{ $client->description }}"
                                ><i class="icon-note"></i>
                                </button>
                            </td>
                            <td>
                                <form action="{{ url('/client/'.$client->id) }}" method="post">
                                    <input type="hidden" name="_method" value="DELETE">
                                    {{ csrf_field() }}
                                    <button class="btn btn-danger btn-circle" onclick="return confirm('Устгах уу?')" type="submit" title="устгах" ><i class="icon-trash" ></i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <h4 class="modal-title text-white" id="myLargeModalLabel">Клиент нэмэх</h4>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="skin skin-square">
                        <form class="form" action="{{ url('client') }}" method="post">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label>Тайлбар</label>
                                <input type="text" class="form-control" name="description">
                            </div>
                            <div class="form-group">
                                <label for="date-input">Хүчинтэй хугацаа :</label>
                                <div class='input-group' >
                                    <input type='date' class="form-control" name="expire" id="date-input" placeholder="yyyy-mm-dd">
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Идэвхжүүлэх эсэх</label>
                                <select class="form-control" name="is_active">
                                    <option value="1">тийм</option>
                                    <option value="0">үгүй</option>
                                </select>
                            </div>
                            <div class="form-group row">
                                <div class="offset-sm-6 col-sm-3">
                                    <button class="btn btn-info waves-effect text-left" type="submit">нэмэх</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <h4 class="modal-title text-white">Клиент засах</h4>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="skin skin-square">
                        <form class="form" action="" method="post" id="form">
                            <input type="hidden" name="_method" value="PUT">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label>Тайлбар</label>
                                <input type="text" class="form-control" name="description" id="description">
                            </div>
                            <div class="form-group">
                                <label for="expire_date">Хүчинтэй хугацаа</label>
                                <div class='input-group'>
                                    <input type='date' class="form-control" name="expire_date" id="expire_date" placeholder="yyyy-mm-dd">
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Идэвхжүүлэх эсэх</label>
                                <select class="form-control" name="is_active" id="is_active">
                                    <option value="1">тийм</option>
                                    <option value="0">үгүй</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <div class="offset-sm-6 col-sm-3">
                                    <button class="btn btn-info waves-effect text-left" type="submit">засах</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('styles')
    <link href="/assets/node_modules/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <style>
        .token{
            cursor: pointer;
        }
    </style>
@endpush
@push('scripts')
    <script src="/assets/node_modules/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>

    <script type="text/javascript">

        $(document).ready(function () {
            $('.token').each(function () {
                var val = $(this).data('val').substring(0,10);
                $(this).text(val+'................');
            });
            $('.token').click(function () {
                if($(this).text().includes('................'))
                    $(this).text($(this).data('val'));
                else
                {
                    var val = $(this).data('val').substring(0,10);
                    $(this).text(val+'................');
                }
            });
        });
    </script>
    <script>
        // $('#datetimepicker-add-close').datepicker({
        //     autoclose: true,
        //     todayHighlight: true,
        //     format: "yyyy-mm-dd"
        // });
        // $('#expire_date').datepicker({
        //     autoclose: true,
        //     todayHighlight: true,
        //     format: "yyyy-mm-dd"
        // });
        $('#modalEdit').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var id = button.data('id')
            var description = button.data('description')
            var is_active = button.data('is_active')
            var expire = button.data('expire')

            var modal = $(this)
            modal.find('.modal-content #form').attr('action', '/client/' + id);
            modal.find('.modal-body input#description').val(description);
            modal.find('.modal-body select#is_active').val(is_active);
            modal.find('.modal-body input#expire_date').val(expire);
        });



    </script>
@endpush