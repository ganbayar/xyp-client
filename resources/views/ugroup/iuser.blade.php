@extends('layouts.iframe')
@section('content')
    <form id="form" target="_top" class="form-horizontal" action="{{ url('ugroup/user/update') }}" method="post">
        {{ csrf_field() }}
        <input type="hidden" name="urgoup_id" value="{{ $ugroup }}">
        <div class="form-group">
            <select id='pre-selected-options' multiple='multiple' class="" name="users[]">
                @foreach($users as $user)
                    @if(in_array($user->id, $selected))
                        <option value="{{ $user->id }}" selected>{{ $user->name }}</option>
                    @else
                        <option value="{{ $user->id }}" >{{ $user->name }}</option>
                    @endif
                @endforeach
            </select>

        </div>
        <div class="form-group row">
            <div class="offset-sm-3 col-sm-9">
                <button class="btn btn-info waves-effect waves-light m-t-10"  type="submit">хадгалах</button>
            </div>
        </div>
    </form>
@endsection