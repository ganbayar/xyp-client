@extends('layouts.iframe')
@section('content')
    <form id="form" target="_top" class="form-horizontal" action="{{ url('ugroup/service/update') }}" method="post">
        {{ csrf_field() }}
        <input type="hidden" name="urgoup_id" value="{{ $ugroup }}">
        <div class="form-group">
            <select id='pre-selected-options' multiple='multiple' class="" name="services[]">
                @foreach($services as $service)
                    @if(in_array($service->id, $selected))
                        <option value="{{ $service->id }}" selected>{{ $service->ws_code }}-{{ $service->title }}</option>
                    @else
                        <option value="{{ $service->id }}" >{{ $service->ws_code }}-{{ $service->title }}</option>
                    @endif
                @endforeach
            </select>

        </div>
        <div class="form-group row">
            <div class="offset-sm-3 col-sm-9">
                <button class="btn btn-info waves-effect waves-light m-t-10"  type="submit">хадгалах</button>
            </div>
        </div>
    </form>
@endsection