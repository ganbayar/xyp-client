@extends('layouts.master')
@section('content')
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Хэрэглэгчийн бүлэг</h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <button data-target="#ugroup-add"  data-toggle="modal" type="button" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Хэрэглэгчийн бүлэг үүсгэх</button>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="table-responsive m-t-40">
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Бүлгийн нэр</th>
                        <th>Хэрэглэгч</th>
                        <th>Хандах эрх</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($ugroups as $ugroup)
                        <tr>
                            <td>{{ $loop->index+1 }}</td>
                            <td>{{ $ugroup->group_name }}</td>
                            <td>
                                <button  type="button"
                                         class="btn btn-info btn-circle"
                                         data-target="#modalframe"
                                         data-toggle="modal"
                                         data-src="{{ url('/ugroup/user/'.$ugroup->id) }}"
                                         data-title="Бүлэгт хэрэглэгч нэмэх"
                                ><i class="icon-settings"></i></button>
                            </td>
                            <td>
                                <button type="button" class="btn btn-info btn-circle"
                                        data-target="#modalframe"
                                        data-toggle="modal"
                                        data-src="{{ url('/ugroup/service/'.$ugroup->id) }}"
                                        data-title="Бүлэгт хандах эрх нэмэх"
                                ><i class="icon-settings"  ></i></button>
                            </td>
                            <td>
                                <button type="button" class="btn btn-info btn-circle btn-sm"
                                        title="засах"
                                        data-target="#ugroup-edit"
                                        data-toggle="modal"
                                        data-id="{{ $ugroup->id }}"
                                        data-name="{{ $ugroup->group_name }}"
                                ><i class="icon-note"  ></i></button>
                            </td>
                            <td>
                                <form action="{{ url('/ugroup/list/'.$ugroup->id) }}" method="post">
                                    <input type="hidden" name="_method" value="DELETE">
                                    {{ csrf_field() }}
                                    <button class="btn btn-danger btn-circle btn-sm" onclick="return confirm('Устгах уу?')" type="submit" title="устгах" ><i class="icon-trash" ></i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="modal fade" id="ugroup-add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <h4 class="modal-title text-white" id="myLargeModalLabel">Хэрэглэгчийн бүлэг үүсгэх</h4>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="skin skin-square">
                        <form class="form" action="{{ url('/ugroup/list') }}" method="post">
                            {{ csrf_field() }}
                            <div class="form-group {{ $errors->has('group_name')?'has-danger':'' }}">
                                <label>Бүлгийн нэр</label>
                                <input type="text" class="form-control {{ $errors->has('group_name')?'form-control-danger':'' }}" name="group_name" required="required">
                                @if($errors->has('group_name'))
                                    @foreach ($errors->get('group_name') as $error)
                                        <small class="form-control-feedback"> {{ $error }} </small>
                                    @endforeach
                                @endif
                            </div>
                            <div class="form-group row">
                                <div class="offset-sm-6 col-sm-3">
                                    <button class="btn btn-info waves-effect text-left" type="submit">үүсгэх</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="ugroup-edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <h4 class="modal-title text-white" id="myLargeModalLabel">Хэрэглэгчийн бүлэг засах</h4>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="skin skin-square">
                        <form class="form" action="" method="post" id="form">
                            <input type="hidden" name="_method" value="PUT">
                            {{ csrf_field() }}
                            <div class="form-group {{ $errors->has('group_name')?'has-danger':'' }}">
                                <label>Бүлгийн нэр</label>
                                <input type="text" class="form-control {{ $errors->has('group_name')?'form-control-danger':'' }}" name="group_name" id="group_name" required="required">
                                @if($errors->has('group_name'))
                                    @foreach ($errors->get('group_name') as $error)
                                        <small class="form-control-feedback"> {{ $error }} </small>
                                    @endforeach
                                @endif
                            </div>
                            <div class="form-group row">
                                <div class="offset-sm-6 col-sm-3">
                                    <button class="btn btn-info waves-effect text-left" type="submit">засах</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

        <div class="modal fade" id="modalframe" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-info">
                        <h4 class="modal-title text-white" id="title"></h4>
                        <button type="button" class="close text-white" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <iframe width="100%" height="400" frameborder="0"></iframe>
                    </div>
                </div>
            </div>
        </div>

@endsection
@push('styles')

@endpush
@push('scripts')
    <script type="text/javascript">
        $('#ugroup-edit').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var id = button.data('id')
            var group_name = button.data('name')

            var modal = $(this)
            modal.find('.modal-content #form').attr('action', '/ugroup/list/' + id);
            modal.find('.modal-body input#group_name').val(group_name);
        });
        $('#modalframe').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var src = button.data('src')
            var title = button.data('title')
            var modal = $(this)
            modal.find('.modal-header #title').html(title);
            modal.find('.modal-body iframe').attr('src', src);
        });
        {{--@if($errors->has('group_name') && $type == 'add')--}}
            {{--$('#ugroup-add').modal({ 'show.bs.modal' : {{ count($errors) > 0 ? true : false }}  });--}}
        {{--@endif--}}
        {{--@if($errors->has('group_name') && $type == 'edit')--}}
        {{--$('#ugroup-edit').modal({ 'show.bs.modal' : {{ count($errors) > 0 ? true : false }}  });--}}
        {{--@endif--}}
    </script>
@endpush