<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['auth']], function () {

    Route::get('/', 'MainController@index');
    Route::resource('group','GroupController');
    Route::prefix('/user/')->group(function () {
        Route::resource('list','UserController');
        Route::get('ugroup/{id}','UserController@getUgroup');
        Route::post('ugroup/update','UserController@ugroupUpdate');
    });
    Route::prefix('/ugroup/')->group(function () {
        Route::resource('list','UgroupController');
        Route::get('user/{id}','UgroupController@getUser');
        Route::post('user/update','UgroupController@userUpdate');
        Route::get('service/{id}','UgroupController@getService');
        Route::post('service/update','UgroupController@serviceUpdate');
    });

    Route::resource('client','ClientController');
    Route::get('log','LogController@index');
    Route::get('service_log','LogController@ServiceLog');

    Route::get('service', 'ServiceController@index');
    Route::post('service', 'ServiceController@index');
    Route::get('service/input', 'ServiceController@serviceInput');
    Route::post('service/input', 'ServiceController@serviceInput');
    Route::get('service/output', 'ServiceController@serviceCall');
    Route::get('service/client/lookup', 'ServiceController@lookupClient');
    Route::post('service/client/lookup', 'ServiceController@lookupClient');
    Route::get('get-pdf', 'ServiceController@makePdf');
});

Auth::routes();

Route::group(['middleware' => ['ApiAuth']], function () {
    Route::get('api/log', 'ApiController@log');
    Route::post('api/xyp/proxy', 'ApiController@anket');
});

Route::get('wrong', function () {
    return view('wrong');
});

Route::get('user/bootstrap', 'UserController@bootstrapUser');
