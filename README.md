# Client web app for XYP data exchange

## Deployment
```bash
git clone git@gitlab.com:tuvshuud1/xyp-client.git
cd xyp-client
docker-compose up -d
curl -vvv localhost:8001
```

## Create initial user
Дараах комманд нь `APP_ENV` тохиргоо demo үед `demo` нэртэй хэрэглэгч үүсгэнэ. Харин `APP_ENV != demo` үед `superuser` нэртэй хэрэглэгч үүсгэнэ.  
Нууц үг: `qwe123!@#`

```bash
docker exec -it xyp-client_web_1 curl -vvv http://localhost:8001/user/bootstrap
```


***API Log query***
----
  ХУР клиент системд үлдсэн лог бүртгэлийн мэдээллийг авах веб сервис

* **URL**

  https://xyp.invescore.mn/api/log?parameters

* **Method:**

  `GET`
  
*  **URL Params**
    
    | Name           | Format |  Description | Required |
    | -------------- |--------| ----------   | -------- | 
    | citizenRegnum  | String | Үйлчлүүлэгчийн РД   | No |
    | operatorRegnum | String | Үйлчилгээний ажилтны РД | No |
    | webserviceCode | String | XYP service code | No |  
    | sinceTimestamp | YYYY-MM-DD HH:MM:SS | Лог шүүлтүүр хийх эхлэх огноо | No |
    | untilTimestamp | YYYY-MM-DD HH:MM:SS | Лог шүүлтүүр хийх төгсгөл огноо | No |

* **Header params**
  
    | Name  | Format | Description | Required |
    | ------|--------|-------------|----------|
    | accessToken | String | Токен дугаарыг тус системийн админаас авна | Yes |

* **Success Response:**
  * **Code:** 200 <br />
    **Content:** 
    ```
    { 
        requestId : 'f599fc5f-caad-4af3-8466-84a625dccde4',  
        requestTimestamp: 1545567132,
        operatorRegnum: 'хп910040020',
        citizenRegnum: 'аю90020040',
        webserviceCode: 'WS100100x',
        billedAmount: 0.00,
        rawData: {}, //Response from XYP data exchange
    }
    ```
 
* **Error Response:**


* **Sample Call:**

  `curl -vvv -H 'accessToken: <your_token>' https://xyp.invescore.mn/api/log?citizenRegnum=АЮ90002340`

* **Notes:**

  `rawData` дээрх өгөгдлийн бүтэц нь ХУР системд бүртгэлтэй тухайн веб сервисийн буцаах утга тул өгөгдлийн бүтцийг developer.xyp.gov.mn цахим хуудас дээрээс үзнэ үү.
 
***API XYP proxy***
----
  ХУР системд хүсэлтийг уламжлах proxy веб сервис

* **URL**

  https://xyp.invescore.mn/api/xyp/proxy

* **Method:**

  `POST`
  
*  **POST Params**

    | Name           | Format |  Description | Required |
    | -------------- |--------| ----------   | -------- | 
    | regnum  | String | Үйлчлүүлэгчийн РД   | Yes |
    | operator_regnum | String | Үйлчилгээний ажилтны РД | Yes |
    | citizen_fingerprint | Base64 encoded image | Иргэний хурууны хээ | Yes |  
    | operator_fingerprint | Base64 encoded image | Үйлчилгээний ажилтны хурууны хээ | Yes |
    | plateNumber | [\w{3}\d{4}] | Тээврийн хэрэгслийн дугаар | No |
    | startYear | YYYY | Нийгмийн даатгалын лавлагааны эхлэх жил | No |
    | endYear | YYYY | Нийгмийн даатгалын лавлагааны төгсгөл жил | No |
    | year | YYYY | Татварын огноо | No |
    | orgId | Integer | Байгууллагын дугаар | No |
    | tin |  | Татварын TIN дугаар | No |
    | legalEntityNumber | String | Хуулийн этгээдийн дугаар | No |
    | propertyNumber | String | Үл хөдлөх хөрөнгийн дугаар | No |

* **Success Response:**
  * **Code:** 200 <br />
    **Content:** 
    ```
    { 
        requestId : 'f599fc5f-caad-4af3-8466-84a625dccde4',  
        requestTimestamp: 1545567132,
        resultMessage: 'Амжилттай',
        resultCode: 0,
        citizenRegnum: 'аю90020040',
        webserviceCode: 'WS100100x',
        result: {}, //Response from XYP data exchange
    }
    ```
 
* **Error Response:**


* **Sample Call:**

  `curl -vvv -H 'accessToken: <your_token>' -d 'regnum=<regnum>&operator_regnum=<operator_regnum>&citizen_fingerprint=<citizen_fingerprint>...' -XPOST https://xyp.invescore.mn/api/xyp/proxy`

* **Notes:**

  `result` дээрх өгөгдлийн бүтэц нь ХУР системд бүртгэлтэй тухайн веб сервисийн буцаах утга тул өгөгдлийн бүтцийг developer.xyp.gov.mn цахим хуудас дээрээс үзнэ үү.
