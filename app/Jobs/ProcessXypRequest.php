<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Library\XypClient;
use App\Models\Service;
use App\Library\LogWrite;
use Zumba\JsonSerializer\JsonSerializer;

class ProcessXypRequest implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $xypClient;
    protected $param;
    protected $serviceSession;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(XypClient $xypClient, $serviceSession, $param)
    {
        $this->xypClient = $xypClient;
        $this->param = $param;
        $this->serviceSession = $serviceSession;
    }

    /**
     * Execute the job.
     *
     */
    public function handle()
    {
        $result = $this->xypClient->request($this->param['ws_code'], $this->param['input_parameter']);

        $serializer = new JsonSerializer();

        $service = Service::where('ws_code', $this->param['ws_code'])->first();
        if(isset($result->requestId)){
            LogWrite::Log(
                $this->serviceSession->getCitizen()['regnum'],
                $this->param['operator_regnum'],
                $result->requestId,
                $serializer->serialize($result->response),
                $this->param['ws_code'],
                $result->resultCode,
                $service->transaction_fee
            );
        }else{
            LogWrite::Log(
                $this->serviceSession->getCitizen()['regnum'],
                $this->param['operator_regnum'],
                '',
                '',
                $this->param['ws_code'],
                2,
                $service->transaction_fee
            );
        }

        return view("service.webservices.$this->param['ws_code']", ['response' => $result->response, 'request' => $result->request]);
    }
}
