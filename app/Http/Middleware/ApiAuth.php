<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Client;
use Illuminate\Http\Request;

class ApiAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $token = $request->header('accessToken');
        if(Client::where('access_token', $token)
		->where('is_active','1')
		->where('expire_date','>',date('Y-m-d'))->exists()){
            return $next($request);
        }else{
            return redirect('wrong');
        }
    }
}
