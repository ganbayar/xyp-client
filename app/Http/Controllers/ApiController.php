<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Log;
use App\Library\ServiceSession;
use App\Library\XypClient;
use App\Models\Service;
use Zumba\JsonSerializer\JsonSerializer;
use App\Library\LogWrite;

class ApiController extends Controller
{
    public function log(Request $request){
        $operatorRegnum = $request->input('operatorRegnum') ? $request->input('operatorRegnum') : '';
        $citizenRegnum = $request->input('citizenRegnum') ? $request->input('citizenRegnum') : '';
        $wsCode = $request->input('webserviceCode') ? $request->input('webserviceCode') : '';
        $startTime = $request->input('sinceTimestamp') ? $request->input('sinceTimestamp') : date('2018-09-06 00:00:00');
        $endTime = $request->input('untilTimestamp') ? $request->input('untilTimestamp') : date('Y-m-d H:i:s');
        $results = Log::where('operator_regnum','like','%'.$operatorRegnum.'%')
            ->where('citizen_regnum','like','%'.$citizenRegnum.'%')
            ->where('wsdl','like','%'.$wsCode.'%')
            ->whereBetween('request_timestamp',[date('Y-m-d H:i:s',strtotime($startTime)),date('Y-m-d H:i:s',strtotime($endTime))])->get();
        $data = [];
        foreach($results as $result){
            $data[]=array(
                'requestId'=> $result->request_id,
                'requestTimestamp' => $result->request_timestamp,
                'operatorRegnum' => $result->operator_regnum,
                'citizenRegnum' => $result->citizen_regnum,
                'webserviceCode' => $result->wsdl,
                'billedAmount' => $result->billed_amount,
                'rawData' => $result->raw_data
            );
        }
        return response()->json([
            $data
        ]);
    }
    public function anket(Request $request){
        $service_list = [];
        if($request->input('ws_codes')){
            $services = Service::where('is_enabled',1)->whereIn('ws_code',$request->input('ws_codes'))->get();
        }else{
            $services = Service::where('is_enabled',1)->get();
        }

        foreach ($services as $service){
            $service_list[]=$service->ws_code;
        }
        $serviceSession = new ServiceSession();
        $serviceSession->setServiceInput($service_list);
        foreach($serviceSession->getInputList() as $ws_code => $input){
            foreach($input as $i){
                $i->setValue($request->input($i->getName()));
            }
        }

        $serviceSession->setAuthCitizen($request->input('regnum'), $request->input('citizen_fingerprint'));
        $serviceSession->setAuthOperator($request->input('operator_regnum'), $request->input('operator_fingerprint'));
        $response = [];
        foreach($serviceSession->getInputList() as $ws_code=>$ws_input){
            $xypClient = new XypClient($this->getWsdlAddress($ws_code));
            $xypClient->setOperator($serviceSession->getOperator()['regnum'], $serviceSession->getOperator()['fingerprint']);
            $xypClient->setCitizen($serviceSession->getCitizen()['regnum'], $serviceSession->getCitizen()['fingerprint']);
            $service = Service::where('ws_code', $ws_code)->first();
            $data = $xypClient->request($ws_code, $serviceSession->getInputParamForRequest($ws_code));
            $serializer = new JsonSerializer();
            if(isset($data->requestId)){
                LogWrite::Log(
                    $serviceSession->getCitizen()['regnum'],
                    $request->session()->get('operator_regnum'),
                    $data->requestId,
                    $serializer->serialize($data),
                    $ws_code,
                    $data->resultCode,
                    $service->transaction_fee
                );

                $response[]=array(
                    'requestId'=> $data->requestId,
                    'resultMessage' => $data->resultMessage,
                    'result' => $serializer->serialize($data),
                    'citizenRegnum' => $serviceSession->getCitizen()['regnum'],
                    'webserviceCode' => $ws_code,
                    'resultCode' => $data->resultCode
                );
            }else{
                LogWrite::Log(
                    $serviceSession->getCitizen()['regnum'],
                    $request->session()->get('operator_regnum'),
                    '',
                    '',
                    $ws_code,
                    2,
                    $service->transaction_fee
                );

                $response[]=array(
                    'requestId'=> '',
                    'resultMessage' => $data->resultMessage,
                    'result' => '',
                    'citizenRegnum' => $serviceSession->getCitizen()['regnum'],
                    'webserviceCode' => $ws_code,
                    'resultCode' => $data->resultCode
                );
            }
        }

        return response()->json([
            $response
        ]);

    }
    private function getWsdlAddress($ws_code){
        if(strpos($ws_code,'WS1001') !== false){
            return 'https://xyp.gov.mn/citizen-1.2.0/ws?WSDL';
        }
        if(strpos($ws_code,'WS1002') !== false){
            return 'https://xyp.gov.mn/property-1.2.0/ws?WSDL';
        }
        if(strpos($ws_code,'WS1003') !== false){
            return 'https://xyp.gov.mn/legal-entity-1.2.0/ws?WSDL';
        }
        if(strpos($ws_code,'WS1004') !== false){
            return 'https://xyp.gov.mn/transport-1.2.0/ws?WSDL';
        }
        if(strpos($ws_code,'WS1005') !== false){
            return 'https://xyp.gov.mn/insurance-1.2.0/ws?WSDL';
        }
        if(strpos($ws_code,'WS1006') !== false){
            return 'https://xyp.gov.mn/tax-1.2.0/ws?WSDL';
        }
    }
}
