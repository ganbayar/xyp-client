<?php

namespace App\Http\Controllers;

use App\Library\ServiceSession;
use App\Library\XypClient;
use App\Models\Service;
use Illuminate\Http\Request;
use App\Models\Group;
use Auth;
use Validator;
use App\Library\LogWrite;
use App\Models\User;
use Zumba\JsonSerializer\JsonSerializer;
use PDF;

// service group -> input -> authorization -> output
class ServiceController extends Controller
{
    // choose service group or create temporary service group
    public function index(Request $request){
        if($request->isMethod('post')){
            if(Auth::user()->regnum){
                $request->session()->put('operator_regnum', Auth::user()->regnum);
            }else{
                return redirect('/service')->with('error', 'Таны регистрийн дугаар бүртгэлгүй байна');
            }
            $service_list = $request->input('service_set');
            $service_group_id = $request->input('service_group_id');

            if(!$service_list && !$service_group_id){
                return redirect('/service')->with('error', 'Үйлчилгээ сонгогдоогүй байна');
            }
            if($service_group_id){
                // check if service group is available
                try{
                    $serviceGroup = Group::findOrFail($service_group_id);

                }catch(\Exception $ex){
                    return response('Invalid request', 400);
                }
                // if service group is available, create assoc array depending on services
                foreach($serviceGroup->services as $service){
                    $service_list[] = $service->ws_code;
                }
            }
            $perm_list=[];
            foreach ($service_list as $service){
                foreach (Auth::user()->ugroups as $ugroup){
                    foreach ($ugroup->services as $webservice){
                        if($service == $webservice->ws_code){
                            $perm_list[] = $webservice->ws_code;
                        }
                    }
                }
            }

            if(empty($perm_list)){
                return redirect('/service')->with('error', 'Хандах эрх байхгүй байна.');
            }

            if(in_array('WS100202_getPropertyList', $perm_list) or in_array('WS100406_getCitizenVehicleList', $perm_list)){
                $forRedirect = [];
                $forRedirect['perm_list'] = $perm_list;
                $forRedirect['service_group_id'] = $service_group_id;
                $request->session()->put('forRedirect', $forRedirect);
            }

            $serviceSession = new ServiceSession();
            $serviceSession->setGroupId($service_group_id);
            $serviceSession->setServiceInput($perm_list);

            $request->session()->flash('serviceSession', $serviceSession);
            return redirect('/service/input')->with('success','Хүсэлт амжилттай');
        }
        $service_group_list = Group::all();
        $webservice_list = Service::where('is_enabled', 1)->get();
        return view('service.group', ['groups'=>$service_group_list, 'webservices'=>$webservice_list]);
    }

    public function serviceInput(Request $request){
        $errorMessages = [
            'required' => 'Мэдээлэл заавал бөглөх шаардлагатай',
            'max' => 'Өгөгдлийн урт хэтэрсэн байна',
            'regex' => 'Мэдээллийн формат таарахгүй байна',
            'alpha_num' => 'Зөвхөн тоо, үсэг оруулна уу',
            'numeric' => 'Зөвхөн тоон утга оруулна уу',
            'date_format' => 'Оролтын утга буруу байна'
        ];

        if($request->input('isReDirect') == 1){
            if(!$request->session()->has('forRedirect')){
                return redirect('/service')->with('error','Алдаа гарлаа');
            }
            $forRedirect = $request->session()->get('forRedirect');
            if(!$forRedirect['fingerPrint']){
                return redirect('/service')->with('error','Алдаа гарлаа');
            }
            //хурууны хээний мэдээллийг $request - руу нэмэв.
            $request->merge(['citizen_fingerprint' => $forRedirect['fingerPrint']]);
            if($request->session()->has('inputVars')){
                $request->merge($request->session()->get('inputVars'));
            }

            $inputVars = [];

            foreach($forRedirect['perm_list'] as $key => $ws_code){
                if($ws_code == 'WS100202_getPropertyList' && $request->input('ws_code') == 'WS100201_getPropertyInfo'){
                    $forRedirect['perm_list'][$key] = $request->input('ws_code');
                }
                if($ws_code == 'WS100406_getCitizenVehicleList' && $request->input('ws_code') == 'WS100401_getVehicleInfo'){
                    $forRedirect['perm_list'][$key] = $request->input('ws_code');
                }
            }

            if(in_array('WS100202_getPropertyList', $forRedirect['perm_list']) or in_array('WS100406_getCitizenVehicleList', $forRedirect['perm_list'])){
                $request->session()->put('forRedirect', $forRedirect);
                if($request->input('propertyNumber')){
                    $inputVars['propertyNumber'] = $request->input('propertyNumber');
                }
                elseif($request->input('plateNumber')){
                    $inputVars['propertyNumber'] = $request->input('propertyNumber');
                }
                $request->session()->put('inputVars', $inputVars);
            }
            else{
                $request->session()->forget('inputVars');
                $request->session()->forget('forRedirect');
            }

            $serviceSession = new ServiceSession();
            $serviceSession->setGroupId($forRedirect['service_group_id']);
            $serviceSession->setServiceInput($forRedirect['perm_list']);
            $request->session()->flash('serviceSession', $serviceSession);
        }

        $serviceSession = $request->session()->get('serviceSession');
        if(!$request->session()->get('serviceSession')){
            return redirect('/service')->with('error','Алдаа гарлаа');
        }

        if($request->isMethod('post')){

            $validator = Validator::make($request->all(), $serviceSession->getValidatorList(), $errorMessages);
            if($validator->fails()){
                $request->session()->reflash();
                return redirect('/service/input')->withErrors($validator);
            }

            foreach($serviceSession->getInputList() as $ws_code => $input){
                foreach($input as $i){
                    $i->setValue(mb_strtolower($request->input($i->getName())));
                }
           }

            // get operator regnum and fingerprint
            // get citizen regnum and fingerprint
            $serviceSession->setAuthCitizen($request->input('regnum'), $request->input('citizen_fingerprint'));
            if($request->session()->has('operator_fingerprint')){
                $serviceSession->setAuthOperator($request->session()->get('operator_regnum'), $request->session()->get('operator_fingerprint'));
            } else {
                $serviceSession->setAuthOperator($request->session()->get('operator_regnum'), $request->input('operator_fingerprint'));
                $request->session()->put('operator_fingerprint', $request->input('operator_fingerprint'));
            }

            $request->session()->flash('serviceSession', $serviceSession);
            return redirect('/service/output');
        }

        $inputList = [];

        $authRequired = True;
        $authNotRequiredList = ['WS100403_getVehiclePenaltyList'];
        //removing duplicated parameters for the view only
        foreach($serviceSession->getInputList() as $ws_code=>$params){
            foreach($params as $param){
                if(!array_key_exists($param->getName(), $inputList)){
                    $inputList[$param->getName()] = $param;
                }
            }
        }

        // set authorization required to False
        if(count($serviceSession->getInputList()) <= count($authNotRequiredList)){
            foreach($serviceSession->getInputList() as $ws_code=>$params){
                foreach($authNotRequiredList as $auth_ws_code){
                    $authRequired = $auth_ws_code !== $ws_code;
                }
            }
        }
        $request->session()->reflash();
        return view('service.input', [
            'inputList' => $inputList
        ])->with('authRequired', $authRequired);
    }

    public function serviceCall(Request $request){
        $serviceSession = $request->session()->get('serviceSession');
        if(!$serviceSession){
            return redirect('/service');
        }
        $service_output = [];

        foreach($serviceSession->getInputList() as $ws_code=>$ws_input){
            $xypClient = new XypClient($this->getWsdlAddress($ws_code));
            $xypClient->setOperator($serviceSession->getOperator()['regnum'], $serviceSession->getOperator()['fingerprint']);
            $xypClient->setCitizen($serviceSession->getCitizen()['regnum'], $serviceSession->getCitizen()['fingerprint']);
            $service = Service::where('ws_code', $ws_code)->first();
            $result = $xypClient->request($ws_code, $serviceSession->getInputParamForRequest($ws_code));
            $service_output[$ws_code] = [
                'name' => $service->title,
                'result' => $result,
                'regnum' => $serviceSession->getCitizen()['regnum']
            ];
            if($request->session()->has('forRedirect')){
                $forRedirect = $request->session()->get('forRedirect');
                $forRedirect['fingerPrint'] = $serviceSession->getCitizen()['fingerprint'];
                $request->session()->put('forRedirect', $forRedirect);
            }
            $serializer = new JsonSerializer();
            if($result->resultCode == 0){
            LogWrite::Log(
                $serviceSession->getCitizen()['regnum'],
                $request->session()->get('operator_regnum'),
                $result->requestId,
                $serializer->serialize($result->response),
                $ws_code,
                $result->resultCode,
                $service->transaction_fee
                );
            }else{
                LogWrite::Log(
                $serviceSession->getCitizen()['regnum'],
                $request->session()->get('operator_regnum'),
                '',
                '',
                $ws_code,
                2,
                $service->transaction_fee
                );
            }
        }
        $request->session()->put('service_output',$service_output); // for pdf value
        return view('service.output', ['service_output'=>$service_output]);
    }

    public function makePdf(Request $request){
        $service_output = $request->session()->get('service_output');
        $pdf = PDF::loadView('pdf.index', ['service_output'=>$service_output]);
        return $pdf->setPaper('a4')->stream('xyp.pdf');
    }

    private function getWsdlAddress($ws_code){
        if(strpos($ws_code,'WS100301') !== false){
            return 'https://xyp.gov.mn/legal-entity-1.2.1/ws?WSDL';
        }
        if(strpos($ws_code,'WS100202') !== false){
            return 'https://xyp.gov.mn/property-1.2.1/ws?WSDL';
        }
        if(strpos($ws_code,'WS1001') !== false){
            return 'https://xyp.gov.mn/citizen-1.2.0/ws?WSDL';
        }
        if(strpos($ws_code,'WS1002') !== false){
            return 'https://xyp.gov.mn/property-1.2.0/ws?WSDL';
        }
        if(strpos($ws_code,'WS1003') !== false){
            return 'https://xyp.gov.mn/legal-entity-1.2.0/ws?WSDL';
        }
        if(strpos($ws_code,'WS1004') !== false){
            return 'https://xyp.gov.mn/transport-1.2.0/ws?WSDL';
        }
        if(strpos($ws_code,'WS1005') !== false){
            return 'https://xyp.gov.mn/insurance-1.2.1/ws?WSDL';
        }
        if(strpos($ws_code,'WS1006') !== false){
            return 'https://xyp.gov.mn/tax-1.2.0/ws?WSDL';
        }
    }

    public function lookupClient(Request $request){
        if($request->isMethod('post')){

        }
        return view('service.clientLookup');
    }
}
