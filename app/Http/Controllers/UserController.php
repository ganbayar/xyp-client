<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\AdminLog;
use App\Models\Ugroup;
use Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('user.index',[ 'users'=>$users ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->regnum = $request->input('regnum');
        $user->is_superuser = $request->input('perm');
        $user->save();


        if($user->save()){
            AdminLog::writeLog(Auth::user()->id,'User Edit','1','Edited user : '.$id);
            return redirect()->to('/user/list')->with('success','Амжилттай шинэчлэгдлээ');
        }else{
            AdminLog::writeLog(Auth::user()->id,'User Edit','0','Edited user : '.$id);
            return redirect()->to('/user/list')->with('error','Алдаа гарлаа');

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function getUgroup($id){
        $ugroups = Ugroup::all();
        $selected = User::find($id)->ugroups;
        $gselected = []; foreach ($selected as $select){ $gselected[]=$select->id; }
        return view('user.iugroup',['ugroups'=>$ugroups,'user'=>$id,'selected'=>$gselected]);
    }

    public function ugroupUpdate(Request $request){
        $user = User::find($request->input('user_id'));
        $user->ugroups()->detach();
        $user->ugroups()->attach($request->input('ugroups'));
        AdminLog::writeLog(Auth::user()->id,'User group edit','1','Edited User : '.$request->input('user_id'));
        return redirect()->to('/user/list')->with('success','Амжилттай шинэчлэгдлээ');
    }

    public function bootstrapUser(Request $request){
        if($request->ip() !== '127.0.0.1'){
            abort(404);
        }
        $user = new User;
        $user->password = '$2y$12$IHnUkTgbja.Vq.YgH4oYdOhd1bCKmEaNkJLhDLIOGFwJgrUiMGSXK'; //qwe123!@#
        $user->regnum = 'АЮ92000001';
        $user->is_superuser = 1;
        $user->is_ldap = 0;
        if(config('app.env') == 'demo'){
            $user->name = 'Demo User';
            $user->email = 'demo@xyp.example.com';
            $user->username = 'demo';
        } else {
            $user->name = 'Superuser';
            $user->email = 'superuser@xyp.example.com';
            $user->username = 'superuser';
        }
        if($user->save()){
            return $user->username.' is created';
        } else {
            return $user->username.' is not created';
        }
    }
}
