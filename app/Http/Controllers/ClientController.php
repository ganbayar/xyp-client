<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Client;
use App\Models\AdminLog;
use Auth;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = Client::all();

        return view('client.index',['clients'=>$clients]);
    }

    public function store(Request $request)
    {
        $expire = strtotime($request->input('expire'));
        $client = new Client;
        $client->access_token = md5(microtime().'-'.rand(1,100000).'-'.date('Y:m:d h:i:s'));
        $client->description = $request->input('description');
        $client->expire_date = date('Y:m:d H:i:s',$expire);
        $client->is_active = $request->input('is_active');
        if($client->save()){
            AdminLog::writeLog(Auth::user()->id,'Client add','1','Added Client : '.$client->id);
            return redirect()->to('client')->with('success','Амжилттай нэмэгдлээ');
        }else{
            AdminLog::writeLog(Auth::user()->id,'Client add','0','Added Client : '.$client->id);
            return redirect()->to('client')->with('error','Алдаа гарлаа');
        }
    }

    public function update(Request $request, $id)
    {
        $expire = strtotime($request->input('expire_date'));
        $client = Client::find($id);
        $client->expire_date = date('Y:m:d H:i:s',$expire);
        $client->description = $request->input('description');
        $client->is_active = $request->input('is_active');
        if($client->save()){
            AdminLog::writeLog(Auth::user()->id,'Client edit','1','Edited Client : '.$client->id);
            return redirect()->to('client')->with('success','Амжилттай шинэчлэгдлээ');
        }else{
            AdminLog::writeLog(Auth::user()->id,'Client edit','0','Edited Client : '.$client->id);
            return redirect()->to('client')->with('error','Алдаа гарлаа');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $client = Client::find($id);
        if($client->delete()){
            AdminLog::writeLog(Auth::user()->id,'Client delete','1','Deleted Client : '.$id);
            return redirect()->to('client')->with('success','Амжилттай устлаа');
        }else{
            AdminLog::writeLog(Auth::user()->id,'Client delete','0','Deleted Client : '.$id);
            return redirect()->to('client')->with('error','Алдаа гарлаа');
        }
    }
}
