<?php

namespace App\Http\Controllers;

use App\Models\Group;
use App\Models\Service;
use Illuminate\Http\Request;
use App\Models\Ugroup;
use App\Models\User;
use App\Models\AdminLog;
use Auth;
use Validator;

class UgroupController extends Controller
{
    public function index()
    {
        $ugroups = Ugroup::all();
        $users = User::all();
        return view('ugroup.index',['ugroups'=>$ugroups,'users'=>$users]);
    }

    public function store(Request $request)
    {
        $messages = [
            'required' => 'Заавал бөглөх шаардлагатай',
            'max' => 'Оролтын урт хэтэрсэн байна'
        ];
        $validator =  Validator::make($request->all(), [
            'group_name' => 'required|max:255'
        ],$messages);

        if ($validator->fails()) {
            return redirect('ugroup/list')
                ->withErrors($validator)
                ->withInput()->with(['error'=>'Алдаа гарлаа','type'=>'add']);
        }
        $ugroup = new Ugroup;
        $ugroup->group_name = $request->input('group_name');
        $ugroup->save();
        AdminLog::writeLog(Auth::user()->id,'Ugroup add','1','Added Ugroup : '.$ugroup->id);
        return redirect()->to('ugroup/list')->with('success','Амжилттай нэмэгдлээ');
    }
    public function update(Request $request, $id)
    {
        $messages = [
            'required' => 'Заавал бөглөх шаардлагатай',
            'max' => 'Оролтын урт хэтэрсэн байна'
        ];
        $validator =  Validator::make($request->all(), [
            'group_name' => 'required|max:255'
        ],$messages);

        if ($validator->fails()) {
            return redirect('ugroup/list')
                ->withErrors($validator)
                ->withInput()->with(['error'=>'Алдаа гарлаа','type'=>'edit','id'=>$id]);
        }
        $ugroup = Ugroup::find($id);
        $ugroup->group_name = $request->input('group_name');
        if($ugroup->save()){
            AdminLog::writeLog(Auth::user()->id,'Ugroup edit','1','Edited Ugroup : '.$ugroup->id);
            return redirect()->to('ugroup/list')->with('success','Амжилттай шинэчлэгдлээ');
        }else{
            AdminLog::writeLog(Auth::user()->id,'Ugroup edit','0','Edited Ugroup : '.$ugroup->id);
            return redirect()->to('ugroup/list')->with('error','Алдаа гарлаа');
        }
    }

    public function destroy($id)
    {
        $ugroup = Ugroup::find($id);
        $ugroup->users()->detach();
        $ugroup->services()->detach();
        if($ugroup->delete()){

            AdminLog::writeLog(Auth::user()->id,'Ugroup delete','1','Deleted Ugroup : '.$id);
            return redirect()->to('ugroup/list')->with('success','Амжилттай устлаа');
        }else{
            AdminLog::writeLog(Auth::user()->id,'Ugroup delete','0','Deleted Ugroup : '.$id);
            return redirect()->to('ugroup/list')->with('error','Алдаа гарлаа');
        }
    }

    public function getUser($id){
        $users = User::all();

        $selected = Ugroup::find($id)->users;
        $gselected = []; foreach ($selected as $select){ $gselected[]=$select->id; }
        return view('ugroup.iuser',['users'=>$users,'ugroup'=>$id,'selected'=>$gselected]);
    }

    public function userUpdate(Request $request){
        $ugroup = Ugroup::find($request->input('urgoup_id'));

        $ugroup->users()->detach();
        $ugroup->users()->attach($request->input('users'));
        AdminLog::writeLog(Auth::user()->id,'Ugroup user edit','1','Edited Ugroup : '.$request->input('urgoup_id'));
        return redirect()->to('ugroup/list')->with('success','Амжилттай шинэчлэгдлээ');
    }

    public function getService($id){
        $services = Service::all();

        $selected = Ugroup::find($id)->services;
        $gselected = []; foreach ($selected as $select){ $gselected[]=$select->id; }
        return view('ugroup.iservice',['services'=>$services,'ugroup'=>$id,'selected'=>$gselected]);
    }

    public function serviceUpdate(Request $request){
        $ugroup = Ugroup::find($request->input('urgoup_id'));

        $ugroup->services()->detach();
        $ugroup->services()->attach($request->input('services'));
        AdminLog::writeLog(Auth::user()->id,'Ugroup service edit','1','Edited Ugroup : '.$request->input('urgoup_id'));
        return redirect()->to('ugroup/list')->with('success','Амжилттай шинэчлэгдлээ');
    }
}
