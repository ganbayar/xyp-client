<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Group;
use App\Models\ServiceGroup;
use App\Models\Service;
use App\Models\AdminLog;
use Auth;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = Group::all();
        $webs = Service::where('is_enabled', 1)->get();
        return view('group.index',['webservices'=>$webs,'groups'=>$groups]);
    }

    public function store(Request $request)
    {
        $group = new Group;
        $group->title = $request->input('group_name');
        if($group->save()){
            $group->services()->attach($request->input('webs'));
            AdminLog::writeLog(Auth::user()->id,'Group Add','1','Added group : '.$group->id);
            return redirect()->to('group')->with('success','Амжилттай нэмэгдлээ');
        }else{
            AdminLog::writeLog(Auth::user()->id,'Group Add','0','Added group : '.$group->id);
            return redirect()->to('group')->with('error','Алдаа гарлаа');
        }
    }

    public function update(Request $request, $id)
    {
        $group = Group::find($id);
        $group->title = $request->input('group_name');
        if($group->save()){
            $group->services()->detach();
            $group->services()->attach($request->input('webs'));
            AdminLog::writeLog(Auth::user()->id,'Group Add','1','Added group : '.$id);
            return redirect()->to('group')->with('success','Амжилттай шинэчлэгдлээ');
        }else{
            AdminLog::writeLog(Auth::user()->id,'Group Update','0','Updated group : '.$id);
            return redirect()->to('group')->with('error','Алдаа гарлаа');

        }



    }
    public function destroy($id)
    {
        $group = Group::find($id);
        $group->services()->detach();
        if($group->delete()){
            AdminLog::writeLog(Auth::user()->id,'Group Delete','1','Deleted group : '.$id);
            return redirect()->to('group')->with('success','Амжилттай устлаа');
        }else{
            AdminLog::writeLog(Auth::user()->id,'Group Delete','0','Deleted group : '.$id);
            return redirect()->to('group')->with('error','Алдаа гарлаа');
        }

    }
}
