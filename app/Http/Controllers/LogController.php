<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Log;
use App\Models\AdminLog;
use Auth;

class LogController extends Controller
{
    public function index(){
        $AdminLogs = AdminLog::orderBy('insert_time','desc')->get();
        return view('log.log',['AdminLogs'=>$AdminLogs]);
    }
    public function ServiceLog(){
        $logs = Log::select('request_timestamp','request_id','billed_amount','operator_regnum','citizen_regnum','wsdl','status')->orderBy('request_timestamp','asc  ')->get();
        return view('log.service_log',['logs'=>$logs]);
    }
}
