<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Carbon;
use Adldap\Laravel\Facades\Adldap;
use Illuminate\Http\Request;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/welcome';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username()
    {
        return 'username';
    }
    public function login(Request $request)
    {
        if (Auth::attempt($request->only(['username', 'password']))) {

            $user = Auth::user();
            $user->last_login = Carbon::now();
            $user->save();

            return redirect()->to('/')->with('success', 'Амжилттай нэвтэрлээ');
        }

        return redirect()->to('login')->with('error', 'Хэрэглэгчийн нэр эсвэл нууц үг буруу байна');
    }
}
