<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Log;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Group;
use App\Models\ServiceGroup;
use App\Models\Service;
use Auth;
use DB;

class MainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = Group::all();
        $logs = Log::count();
        $log_success = Log::where('status','0')->count();
        $users = User::count();
        $clients = Client::where('is_active',1)->count();
        $log_chart = Log::select(DB::raw("count(0) as cnt, DATE(request_timestamp) as day"))->groupBy(DB::raw("DATE(request_timestamp)"))->get();
        return view('welcome',['users'=>$users,'logs'=>$logs,'clients'=>$clients,'log_success'=>$log_success,'log_chart'=>$log_chart]);
    }
}
