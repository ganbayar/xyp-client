<?php
/**
 * Created by PhpStorm.
 * User: ganbayar
 * Date: 8/27/18
 * Time: 5:36 PM
 */

namespace App\Library;

class ServiceSession {
    private $group_id;
    private $input_list = [];
    private $auth_operator = [];
    private $auth_citizen = [];

    public function addInput($ws_code, $input){
        array_add($this->input_list, $ws_code, $input);
    }

    public function getInputList(){
        return $this->input_list;
    }

    public function setGroupId($group_id){
        $this->group_id = $group_id;
        //
    }

    public function setAuthOperator($regnum, $fingerprint){
        $this->auth_operator['regnum'] = mb_strtolower($regnum);
        $this->auth_operator['fingerprint'] = $fingerprint;
    }

    public function setAuthCitizen($regnum, $fingerprint){
        $this->auth_citizen['regnum'] = mb_strtolower($regnum);
        $this->auth_citizen['fingerprint'] = $fingerprint;
    }

    public function getOperator(){
        return $this->auth_operator;
    }

    public function getCitizen(){
        return $this->auth_citizen;
    }

    public function getValidatorList(){
        $result = [];
        foreach($this->getInputList() as $ws_code=>$input){
            foreach($input as $i){
                $result[$i->getName()] = $i->getValidationRule();
            }
        }
        return $result;
    }
    public function getInputParamForRequest($ws_code){
        $inputParameterObjectList = $this->getInputList()[$ws_code];
        $result = [];
        foreach ($inputParameterObjectList as $i){
            $result[$i->getName()] = $i->getValue();
        }
        return $result;
    }

    //sets required service input parameters
    /*
     * @param service_list // list of WS_CODE
     */
    public function setServiceInput($service_list){
        $regnumField = new ServiceInput("regnum", 'Регистрийн дугаар', 'required|max:10|alpha_num|regex:/^[\x{0430}-\x{044f}\x{0410}-\x{042f}\x{0401}\x{0451}\x{04e8}\x{04e9}\x{04ae}\x{04af}]{2}\-?[0-9]{8}$/u','','АБ12345678');
        $platenumField = new ServiceInput('plateNumber', 'Тээврийн хэрэгслийн дугаар', 'required|max:7|alpha_num', '','1234УНА');

        $inputMap = [
            'WS100611_getVehicleInfo' => [
                $platenumField,
                $regnumField
            ],
            'WS100401_getVehicleInfo' => [
                $platenumField,
                $regnumField
            ],
            'WS100402_getVehicleOwnerHistoryList' => [
                $platenumField,
                $regnumField
            ],
            'WS100403_getVehiclePenaltyList' => [
                $platenumField
            ],
            'WS100501_getCitizenSalaryInfo' => [
                new ServiceInput('startYear', 'Нийгмийн даатгалын лавлагааны эхлэх жил', 'required|numeric|date_format:Y', 'number' ),
                new ServiceInput('endYear', 'Нийгмийн даатгалын лавлагааны төгсгөл жил', 'required|numeric|date_format:Y', 'number'),
                $regnumField
            ],
            'WS100502_getCitizenPensionInquiry' => [
                new ServiceInput('startYear', 'Нийгмийн даатгалын лавлагааны эхлэх жил', 'required|date_format:Y', 'number','',date('Y')-1),
                new ServiceInput('endYear', 'Нийгмийн даатгалын лавлагааны төгсгөл жил', 'required|date_format:Y', 'number','',date('Y')),
                $regnumField
            ],
            'WS100601_getTaxPayerInfo' => [
                new ServiceInput('year', 'Татварын он', 'required|max:2018|min:2000|numeric', 'number'),
                new ServiceInput('tin', 'Байгууллагын регистрийн дугаар', 'required|numeric', 'number', '1234567'),
                new ServiceInput('orgId', '', 'required|numeric', 'hidden','', '151009'),
                new ServiceInput('dedication', '', 'required|string', 'hidden','', 'Инвэскор санхүүгийн байгууллагад'),
                new ServiceInput('documentResponse', '', 'string', 'hidden','', 'Хариу биш'),
                new ServiceInput('dedicationOrg', '', 'required|string', 'hidden','', 'Инвэскор'),
            ],
            'WS100301_getLegalEntityInfo' => [
                new ServiceInput('legalEntityNumber', 'Хуулийн этгээдийн регистрийн дугаар', 'required|numeric', 'number','1234567'),
                $regnumField,
            ],
            'WS100201_getPropertyInfo' => [
                $regnumField,
                new ServiceInput('propertyNumber', 'Үл хөдлөх хөрөнгийн дугаар', 'required|max:11|alpha_num','','ү1234567890'),
            ],
            'WS100202_getPropertyList' => [
                $regnumField
            ],
            'PV100101_lookupClient' => [
                $regnumField
            ]
        ];
        foreach($service_list as $ws_code){
            if(array_key_exists($ws_code, $inputMap)){
                $this->input_list[$ws_code] = $inputMap[$ws_code];
            } else {
                $this->input_list[$ws_code] = [$regnumField];
            }
        }
    }
}
