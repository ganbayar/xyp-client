<?php
namespace App\Library;

class XypClient {
    private $wsdl = "";
    private $signingInfo = null;
    private $soapParam = null;

    function __construct($wsdl){
        $accessToken = config('app.xyp_access_token');
        $keyPath = config('app.xyp_key_path');
        // private key is not required when env is set to demo
        if(config('app.env') != 'demo') {
            $sign = new XypSign($keyPath, $accessToken);
            $this->signingInfo = $sign->sign();
        }
        $this->wsdl = $wsdl;

        $this->soapParam = [
            "auth" => [
                "citizen" => [
                    "fingerprint" => "",       // Иргэний хурууны хээний зураг. 310x310 харьцаатай PNG өртгөлтэй
                    "regnum" => ""             // Иргэний регистрийн дугаар
                ],
                "operator" => [
                    "regnum" => "",            // Үйлчилгээг үзүүлэгч ажилтны регистрийн дугаар
                    "fingerprint" => ""        // Үйлчилгээг үзүүлэгч ажилтны хурууны хээний зураг. 310x310 харьцаатай PNG өртгөлтэй
                ],
            ],
        ];
    }

    function setCitizen($regnum, $fingerprint){
        $this->soapParam["auth"]["citizen"] = [
            "regnum" => $regnum,
            "fingerprint" => base64_decode($fingerprint)
        ];
        return $this;
    }

    function setOperator($regnum, $fingerprint){
        $this->soapParam["auth"]["operator"] = [
            "regnum" => $regnum,
            "fingerprint" => base64_decode($fingerprint)
        ];
        return $this;
    }

    function request($operationName, $params){
        if(config('app.env') == 'demo'){
            $response = json_decode(file_get_contents(resource_path('/demo/'.$operationName.'.json')));
            return $response;
        }else{
            $client = new \SoapClient(
                $this->wsdl,
                [
                    'soapVersion' => SOAP_1_2,
                    'stream_context' => stream_context_create([
                        'ssl' => [
                            'verify_peer' => false,
                            'allow_self_signed' => true
                        ],
                        'http' => [
                            'header' =>
                                sprintf("accessToken: %s\r\n", $this->signingInfo['accessToken']).
                                sprintf("timeStamp: %s\r\n", $this->signingInfo['timeStamp']).
                                sprintf("signature: %s", $this->signingInfo['signature'])
                        ]
                    ])
                ]
            );
            $this->soapParam = [
                "request" => array_merge($this->soapParam, $params)
            ];
            $response = $client->$operationName($this->soapParam);
            return $response->return;

        }
    }
}
