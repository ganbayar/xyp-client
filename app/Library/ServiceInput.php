<?php
/**
 * Created by PhpStorm.
 * User: ganbayar
 * Date: 8/28/18
 * Time: 4:04 PM
 */

namespace App\Library;


class ServiceInput {
    private $name;
    private $label;
    private $validationRule;
    private $value;
    private $type = "text";
    private $placeHolder;

    function __construct($name="", $label="", $validationRule="", $type="text",$placeHolder="",$value=""){
        $this->setName($name);
        $this->setLabel($label);
        $this->setValidationRule($validationRule);
        $this->setType($type);
        $this->setPlaceHolder($placeHolder);
        $this->setValue($value);
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param mixed $label
     */
    public function setLabel($label): void
    {
        $this->label = $label;
    }

    public function setValidationRule($validationRule){
        $this->validationRule = $validationRule;
    }

    public function getValidationRule(){
        return $this->validationRule;
    }

    public function setPlaceHolder($placeHolder){
        $this->placeHolder = $placeHolder;
    }
    public function getPlaceHolder(){
        return $this->placeHolder;
    }
    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value): void
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }
}