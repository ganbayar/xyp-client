<?php

namespace App\Library;

use Illuminate\Database\Eloquent\Model;

class LogWrite extends Model
{
    protected $table = 'log';

    public $incrementing = false;

    public $timestamps = false;

    public static function Log($citizen_regnum,$operator_regnum,$request_id,$content,$wsdl,$status,$bill){
        $log = new LogWrite;
        $log->citizen_regnum = $citizen_regnum;
        $log->operator_regnum = $operator_regnum;
        $log->request_id = $request_id;
        $log->request_timestamp = date('Y-m-d H:i:s');
        $log->raw_data = $content;
        $log->wsdl = $wsdl;
        $log->status = $status;
        $log->billed_amount = $bill;
        $log->save();

        return true;
    }
}
