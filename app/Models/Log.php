<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 25 Aug 2018 06:13:20 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Log
 * 
 * @property int $id
 * @property \Carbon\Carbon $request_timestamp
 * @property string $request_id
 * @property float $billed_amount
 * @property string $raw_data
 * @property int $operator_id
 * @property string $operator_regnum
 * @property string $citizen_regnum
 * @property int $service_id
 * 
 * @property \App\Models\Service $service
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class Log extends Eloquent
{
	protected $table = 'log';
	public $timestamps = false;

	protected $casts = [
		'billed_amount' => 'float'
	];

	protected $dates = [
		'request_timestamp'
	];

	protected $fillable = [
		'request_timestamp',
		'request_id',
		'billed_amount',
		'raw_data',
		'operator_regnum',
		'citizen_regnum',
		'wsdl'
	];
}
