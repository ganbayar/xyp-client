<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 25 Aug 2018 06:13:20 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Service
 * 
 * @property int $id
 * @property string $title
 * @property string $ws_code
 * @property float $transaction_fee
 * @property int $is_enabled
 * 
 * @property \Illuminate\Database\Eloquent\Collection $logs
 * @property \Illuminate\Database\Eloquent\Collection $groups
 *
 * @package App\Models
 */
class Service extends Eloquent
{
	protected $table = 'service';
	public $timestamps = false;

	protected $casts = [
		'transaction_fee' => 'float',
		'is_enabled' => 'int'
	];

	protected $fillable = [
		'title',
		'ws_code',
		'transaction_fee',
		'is_enabled'
	];

	public function logs()
	{
		return $this->hasMany(\App\Models\Log::class);
	}

	public function groups()
	{
		return $this->belongsToMany(\App\Models\Group::class, 'service_group')
					->withPivot('id');
	}
}
