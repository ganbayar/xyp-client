<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserUgroup extends Model
{
    protected $table = 'user_ugroup';
    public $timestamps = false;

    protected $casts = [
        'user_id' => 'int',
        'ugroup_id' => 'int'
    ];

    protected $fillable = [
        'user_id',
        'ugroup_id'
    ];

    public function user()
    {
        return $this->belongsTo(\App\Models\Group::class);
    }

    public function ugroup()
    {
        return $this->belongsTo(\App\Models\Service::class);
    }
}
