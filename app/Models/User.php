<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 25 Aug 2018 06:13:20 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;

/**
 * Class User
 * 
 * @property int $id
 * @property string $firstname
 * @property string $lastname
 * @property string $ldap_user
 * @property \Carbon\Carbon $last_login
 * @property string $regnum
 * @property int $is_superuser
 * 
 * @property \Illuminate\Database\Eloquent\Collection $logs
 *
 * @package App\Models
 */
class User extends Eloquent implements Authenticatable
{
    use AuthenticableTrait;
	protected $table = 'users';

	protected $casts = [
		'is_superuser' => 'int'
	];

	protected $dates = [
		'last_login'
	];

	protected $fillable = [
		'name',
		'lastname',
		'ldap_user',
		'last_login',
		'regnum',
		'is_superuser',
        'username'
	];

	public function logs()
	{
		return $this->hasMany(\App\Models\Log::class, 'operator_id');
	}
    public function ugroups()
    {
        return $this->belongsToMany(\App\Models\Ugroup::class, 'user_ugroup')
            ->withPivot('id');
    }
}
