<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 25 Aug 2018 06:13:20 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ServiceGroup
 * 
 * @property int $id
 * @property int $group_id
 * @property int $service_id
 * 
 * @property \App\Models\Group $group
 * @property \App\Models\Service $service
 *
 * @package App\Models
 */
class ServiceGroup extends Eloquent
{
	protected $table = 'service_group';
	public $timestamps = false;

	protected $casts = [
		'group_id' => 'int',
		'service_id' => 'int'
	];

	protected $fillable = [
		'group_id',
		'service_id'
	];

	public function group()
	{
		return $this->belongsTo(\App\Models\Group::class);
	}

	public function service()
	{
		return $this->belongsTo(\App\Models\Service::class);
	}
}
