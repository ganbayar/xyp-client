<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminLog extends Model
{
    protected $table = 'admin_log';

    public $incrementing = false;

    public $timestamps = false;

    public static function writeLog($user_id,$operation,$status,$message){
        $log = new AdminLog;
        $log->operation = $operation;
        $log->user_id = $user_id;
        $log->status = $status;
        $log->description = $message;
        $log->insert_time = date('Y-m-d H:i:s');
        $log->save();

        return true;
    }
}
