<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UgroupService extends Model
{
    protected $table = 'ugroup_service';
    public $timestamps = false;

    protected $casts = [
        'service_id' => 'int',
        'ugroup_id' => 'int'
    ];

    protected $fillable = [
        'service_id',
        'ugroup_id'
    ];

    public function service()
    {
        return $this->belongsTo(\App\Models\Group::class);
    }

    public function ugroup()
    {
        return $this->belongsTo(\App\Models\Service::class);
    }
}
