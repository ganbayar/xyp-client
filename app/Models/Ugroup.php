<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ugroup extends Model
{
    protected $table = 'ugroup';
    public $timestamps = false;

    protected $fillable = [
        'group_name'
    ];

    public function users()
    {
        return $this->belongsToMany(\App\Models\User::class, 'user_ugroup')
            ->withPivot('id');
    }

    public function services()
    {
        return $this->belongsToMany(\App\Models\Service::class, 'ugroup_service')
            ->withPivot('id');
    }
}
