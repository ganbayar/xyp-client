<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 25 Aug 2018 06:13:20 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Group
 * 
 * @property int $id
 * @property string $title
 * 
 * @property \Illuminate\Database\Eloquent\Collection $services
 *
 * @package App\Models
 */
class Group extends Eloquent
{
	protected $table = 'group';
	public $timestamps = false;

	protected $fillable = [
		'title'
	];

	public function services()
	{
		return $this->belongsToMany(\App\Models\Service::class, 'service_group')
					->withPivot('id');
	}
}
