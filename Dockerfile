FROM tuvshuud/php7-apache-laravel:latest

ADD ./ /tmp/docker/

RUN cp /tmp/docker/composer.json /var/www/html/composer.json \
    && cp /tmp/docker/.env /var/www/html/.env \
    && cp -r /tmp/docker/app/Library /var/www/html/app/Library \
    && composer update -d /var/www/html

RUN apt update \
	&& apt install -y wkhtmltopdf xauth xvfb

RUN apt install -y ca-certificates \
    && curl -k -o /usr/local/share/ca-certificates/MNRCA.cer https://esign.gov.mn/MNRCA.cer \
    && curl -k -o /usr/local/share/ca-certificates/MNICA.cer https://esign.gov.mn/MNICA-2018.cer \
    && update-ca-certificates

RUN apt-get -y autoremove && apt-get clean && rm -rf /var/lib/apt/lists/*

RUN ln -sf /usr/share/zoneinfo/Asia/Ulan_Bator /etc/localtime \
    && chown -R www-data:www-data /var/www/html/bootstrap/cache

EXPOSE 80

CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]
